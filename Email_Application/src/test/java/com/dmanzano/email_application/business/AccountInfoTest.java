    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dmanzano.email_application.business;


import java.util.Arrays;
import java.util.Collection;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

/**
 *
 * @author Danny
 * @version 1.10
 */
@RunWith(Parameterized.class)
public class AccountInfoTest {
    
    @Parameters(name = "{index} plan[{0}]={1}]")
    public static Collection<Object[]> data(){
        return Arrays.asList(new Object[][]{
            {"", "153243234", false},
            {"send.1532483@gmail.com", null, false},
            {null, "", false},
            {"send.1532483@gmail.com", "4753284", true},
            {"send.1532483@gmail.com", "", true}
        });
    }
    
    private String email;
    private String password;
    private boolean successExpected;
    
    public AccountInfoTest(String email, String password, boolean successExpected){
        this.email = email;
        this.password = password;
        this.successExpected = successExpected;
    }
    
    @Test
    public void accountInfoConstructorTest(){
        try{
            UserInfo ai = new UserInfo(this.email, this.password);
            assertTrue(this.successExpected);
        }catch(IllegalArgumentException iae){
            assertFalse(this.successExpected);
        }
    }
}
