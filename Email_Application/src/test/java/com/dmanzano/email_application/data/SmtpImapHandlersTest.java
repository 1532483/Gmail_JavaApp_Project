/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dmanzano.email_application.data;

import com.dmanzano.email_application.beans.AddressBean;
import com.dmanzano.email_application.business.UserInfo;
import com.dmanzano.email_application.beans.EmailBean;
import com.dmanzano.email_application.beans.AttachmentBean;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.Collection;
import static junit.framework.Assert.assertEquals;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

/**
 *
 * @author Danny
 * @version 1.10
 */
@RunWith(Parameterized.class)
public class SmtpImapHandlersTest {

    @Parameters(name = "{index} plan[{0}]={1}]")
    public static Collection<Object[]> data() throws IOException {
        AddressBean[] receivers = new AddressBean[]{new AddressBean("receiver", "receive.1532483@gmail.com")};
        AddressBean[] others = new AddressBean[]{new AddressBean("other", "other.1532483@gmail.com")};
        AddressBean sender = new AddressBean("sender","send.1532483@gmail.com");
        return Arrays.asList(new Object[][]{
            //Valid: Fully Standard Test 0
            {new UserInfo("send.1532483@gmail.com", "1532483sro"), 
                new EmailBean(sender, receivers, 
                others, others, 
                "Test 0", "textMessage", "<b>htmlMessage</b>", new AttachmentBean[0]), true},
            //Valid: Empty Receiver Test 1
            {new UserInfo("send.1532483@gmail.com", "1532483sro"),
                new EmailBean(sender, new AddressBean[0],
                others, others,
                "Test 1", "textMessage", "<b>htmlMessage</b>", new AttachmentBean[0]), true}, 
            //Valid: Empty CC Test 2
            {new UserInfo("send.1532483@gmail.com", "1532483sro"), 
                new EmailBean(sender, receivers, 
                new AddressBean[0], others, 
                "Test 2", "textMessage", "<b>htmlMessage</b>", new AttachmentBean[0]), true},
            //Valid: Empty BCC Test 3
            {new UserInfo("send.1532483@gmail.com", "1532483sro"), 
                new EmailBean(sender, receivers, 
                others, new AddressBean[0], 
                "Test 3", "textMessage", "<b>htmlMessage</b>", new AttachmentBean[0]), true},
            //Invalid: All empty TO,CC, BCC Test 4
            {new UserInfo("send.1532483@gmail.com", "1532483sro"), 
                new EmailBean(sender, new AddressBean[0], 
                new AddressBean[0], new AddressBean[0], 
                "Test 4", "textMessage", "<b>htmlMessage</b>", new AttachmentBean[0]), false},
            //Invalid: Null Sender Test 5
            {new UserInfo("send.1532483@gmail.com", "1532483sro"),
                new EmailBean(null, receivers, 
                others, others, 
                "Test 5", "textMessage", "<b>htmlMessage</b>", new AttachmentBean[0]), false},
            //Valid: Null CC Test 6
            {new UserInfo("send.1532483@gmail.com", "1532483sro"), 
                new EmailBean(sender, receivers, 
                null, others, 
                "Test 6", "textMessage", "<b>htmlMessage</b>", new AttachmentBean[0]), true}, 
            //Valid: Null BCC Test 7
            {new UserInfo("send.1532483@gmail.com", "1532483sro"), 
                new EmailBean(sender, receivers, 
                others, null, 
                "Test 7", "textMessage", "<b>htmlMessage</b>", new AttachmentBean[0]), true}, 
            //Valid: Null Subject Test 8
            {new UserInfo("send.1532483@gmail.com", "1532483sro"), 
                new EmailBean(sender, receivers, 
                others, others, 
                null, "Test 10", "<b>Test 10</b>", new AttachmentBean[0]), true},
            //Valid: Null Receiver Test 9
            {new UserInfo("send.1532483@gmail.com", "1532483sro"), 
                new EmailBean(sender, null, 
                others, others, 
                "Test 11", "textMessage", "<b>htmlMessage</b>", new AttachmentBean[0]), true},
            //Valid: Regular Attachment  Test 10
            {new UserInfo("send.1532483@gmail.com", "1532483sro"), 
                new EmailBean(sender, receivers, 
                others, others, 
                "Test 12", "textMessage", "<b>htmlMessage</b>", new AttachmentBean[]{
                    new AttachmentBean("davis_image.png", 
                        Files.readAllBytes(FileSystems.getDefault().getPath("", "davis_image.png")), false, null)}), true},
            //Valid: Embedded Attachment Test 11
            {new UserInfo("send.1532483@gmail.com", "1532483sro"), 
                new EmailBean(sender, receivers, 
                others, others, 
                "Test 13", "textMessage", "<html><META http-equiv=\"Content-Type\" "
                            + "content=\"text/html; charset=utf-8\"/>"
                            + "<body><h1>Hey Hey Hey</h1><img src='cid:davis_image.png'/><h2>HEY</h2></body></html>"
                    , new AttachmentBean[]{
                    new AttachmentBean("davis_image.png", 
                        Files.readAllBytes(FileSystems.getDefault().getPath("", "davis_image.png")), true, null)}), true}
            //Conclusion for test:
            //*TextMessage and HtmlMessage cannot be null
            //*Unexpected behaviour (is valid?) when not setting sender in email 
                //Valid: Empty Sender Test 
            //Unexpected result: Shouldn't suceed but it does. Teacher will look into it.
            //Unexpected result: When receiving email back the email has the sender set to 'from'
            /*{new UserInfo("send.1532483@gmail.com", "1532483sro"), 
                new EmailBean("", new String[]{"receive.1532483@gmail.com"}, 
                new String[]{"other.1532483@gmail.com"}, new String[]{"other.1532483@gmail.com"}, 
                "Test 5", "textMessage", "<b>htmlMessage</b>", new AttachmentBean[0]), true},*/
        });
    }

    private UserInfo sender;
    private EmailBean email;
    private boolean isExpectedSuccess;
    private UserInfo receiver;
    private UserInfo cc;

    public SmtpImapHandlersTest(UserInfo sender, EmailBean email, boolean isExpectedSuccess) {
        this.sender = sender;
        this.email = email;
        this.isExpectedSuccess = isExpectedSuccess;
        this.receiver = new UserInfo("receive.1532483@gmail.com", "1532483sro");
        this.cc = new UserInfo("other.1532483@gmail.com", "1532483sro");
    }

    @Test
    public void SmtpPerformProtocolTest() {
        boolean isSuccessful = false;
        try {
        SmtpHandler sm = new SmtpHandler();
            if (sm.performProtocol(this.sender, this.email)) {
                ImapHandler imh = new ImapHandler();
                Thread.sleep(5000);
                if (this.email.getReceivers().length != 0) {
                    if (imh.performProtocol(this.receiver)) {
                        EmailBean[] ebArr = imh.getReceivedEmails();
                        isSuccessful = this.email.equals(ebArr[ebArr.length - 1]);
                    }
                }else{
                    if (imh.performProtocol(this.cc)) {
                        EmailBean[] ebArr = imh.getReceivedEmails();
                        isSuccessful = this.email.equals(ebArr[ebArr.length - 1]);
                    }
                    
                }
            }
        } catch (Exception e) {
            isSuccessful = false;
        }
        assertEquals(this.isExpectedSuccess, isSuccessful);
    }

}
