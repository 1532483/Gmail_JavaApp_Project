/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dmanzano.email_application.persistence;


import com.dmanzano.email_application.beans.EmailBean;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import org.junit.Assert;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

/**
 *
 * @author Danny
 */
@Ignore
public class FolderDAOTest {
    
    private final String url = "jdbc:mysql://localhost:3306/JAGDBTest?autoReconnect=true&useSSL=false&allowPublicKeyRetrieval=true";
    private final String user = "creator";
    private final String password = "1532483sro";
    private final String appUser = "send.1532483@gmail.com";
    
    private EmailBean[] ebArr;
    private FolderDAO DAO;
    
    public FolderDAOTest() throws IOException{
        DAO = new FolderDAO(this.url, this.user, this.password, this.appUser);
    }
    
    @Test
    public void getFoldersTest() throws SQLException{
        String[] result = DAO.getFolders();
        Assert.assertArrayEquals(new String[]{"INBOX", "SENT", "TRASH"}, result);
    }
    
    @Test
    public void addFolderTest() throws SQLException{
        assertEquals(true, DAO.addFolder("UNWANTED"));
    }
    
    @Test
    public void removeFolderTest() throws SQLException{
        assertEquals(true, DAO.removeFolder("SENT"));
    }        
    
    @Test 
    public void removeNonExistentFolderTest() throws SQLException{
        assertEquals(false, DAO.removeFolder("SOMETHING"));
    }

    //Happens before every test that is why each test doesn't commit to the DB
    @Before
    public void reconstructDB() {
        String data = loadAsString("CREATE_TABLES_TEST.sql");
        try (Connection connection = DriverManager.getConnection(url, user, password)) {
            for (String statement : splitStatements(new StringReader(data), ";")) {
                connection.prepareStatement(statement).execute();
            }
        } catch (SQLException sqle) {
            throw new RuntimeException("Failed seeding database", sqle);
        }
    }

    //taken from ken's example
    private String loadAsString(String path) {
        try (InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(path)) {
            Scanner scanner = new Scanner(inputStream);
            return scanner.useDelimiter("\\A").next();
        } catch (IOException e) {
            throw new RuntimeException("Unable to close input stream", e);
        }
    }

    //this method is similar to ken's code but I modified it to make it more efficient 
    //StringBuilder is not efficient
    private List<String> splitStatements(Reader reader, String statementDelimiter) {
        BufferedReader bufferedReader = new BufferedReader(reader);
        String sqlStatement = "";
        List<String> stmts = new ArrayList<>();
        try {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                line = line.trim();
                if (line.isEmpty() || isComment(line)) {
                    continue;
                }
                sqlStatement += line;
                if (line.endsWith(statementDelimiter)) {
                    stmts.add(sqlStatement);
                    sqlStatement = "";
                }
            }
            return stmts;
        } catch (IOException e) {
            throw new RuntimeException("Failed parsing sql", e);
        }
    }

    private boolean isComment(String line) {
        return line.startsWith("--");
    }
}
