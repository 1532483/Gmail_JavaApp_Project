/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dmanzano.email_application.persistence;

import com.dmanzano.email_application.beans.AddressBean;
import com.dmanzano.email_application.beans.AttachmentBean;
import com.dmanzano.email_application.beans.EmailBean;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import org.junit.Assert;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Ignore;

/**
 *
 * @author Danny
 * @version 2.4
 */
@Ignore
public class EmailDAOTest {
    private String url = "jdbc:mysql://localhost:3306/JAGDBTest?autoReconnect=true&useSSL=false&allowPublicKeyRetrieval=true";
    private String user = "creator";
    private String password = "1532483sro";

    private EmailBean[] ebArr;
    private EmailDAO DAO;

    public EmailDAOTest() throws IOException, ParseException {
        AddressBean[] receivers = new AddressBean[]{new AddressBean("receiver", "receive.1532483@gmail.com")};
        AddressBean[] senders = new AddressBean[]{new AddressBean("sender","send.1532483@gmail.com")};
        AddressBean[] others = new AddressBean[]{new AddressBean("other", "other.1532483@gmail.com")};
        AddressBean sender = new AddressBean("sender","send.1532483@gmail.com");
        AddressBean receiver = new AddressBean("receiver","receive.1532483@gmail.com");
        DAO = new EmailDAO(url, user, password, sender.getUserAddress());
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        java.util.Date sent = dateFormat.parse("2010-08-02");
        java.util.Date received = dateFormat.parse("2010-08-03");
        ebArr = new EmailBean[]{
            //Bean 0: Valid EmailBean with null receive date
            new EmailBean(sender,
            receivers, others,
            others, "Bean 0",
            "text message 0", "<b>html message 0</b>", new AttachmentBean[]{new AttachmentBean("davis_image.png",
                Files.readAllBytes(FileSystems.getDefault().getPath("", "davis_image.png")), false, null)}, -1, new Timestamp(System.currentTimeMillis()), null, "SENT"),
            //Bean 1: Valid with Embbedded Attachment
            new EmailBean(sender,
            receivers, others,
            others, "Insert Test 1",
            "text message 1", "<html><META http-equiv=\"Content-Type\" "
            + "content=\"text/html; charset=utf-8\">"
            + "<body><h1>Hey Hey Hey</h1><img src='cid:davis_image.png' /><h2>HEY</h2></body></html>",
            new AttachmentBean[]{new AttachmentBean("davis_image.png",
                Files.readAllBytes(FileSystems.getDefault().getPath("", "davis_image.png")), true, null)},
            -1, new Timestamp(System.currentTimeMillis()), null, "SENT"),
            //Bean 2: Email inserted with eid 1 to test equality
            new EmailBean(sender,
            receivers, others,
            others, "First Case",
            "This is some text", "<h1>HEY HEY HEY</h1>",
            new AttachmentBean[0],
            -1, new Timestamp(sent.getTime()), new Timestamp(received.getTime()), "SENT"),
            //Bean 3: Email with receive.1532483@gmail.com as sender
            new EmailBean(receiver,
            senders, others,
            others, "First Case",
            "This is some text", "<h1>HEY HEY HEY</h1>",
            new AttachmentBean[0],
            -1, new Timestamp(sent.getTime()), new Timestamp(received.getTime()), "SENT")

        };
    }

    @Test
    public void insertValidEmailTest() throws SQLException {
        int eid = DAO.insertEmail(ebArr[0]);
        if (eid != -1) {
            EmailBean eb = DAO.findEmailByEid(eid);
            assertEquals(ebArr[0], eb);
        } else {
            fail("Complete insertion was not successful");
        }
    }

    @Test(expected = NullPointerException.class)
    public void insertNullEmailTest() throws SQLException {
        DAO.insertEmail(null);
    }

    @Test
    public void removeEmailTest() throws SQLException {
        int eid = DAO.insertEmail(ebArr[1]);
        if (eid != -1) {
            DAO.removeEmail(eid);
            assertEquals(DAO.findEmailByEid(eid), null);
        } else {
            fail("Complete insertion was not successful");
        }

    }

    @Test
    public void removeNonExistentEmailTest() throws SQLException {
        assertEquals(false, DAO.removeEmail(123432512));
    }

    @Test
    public void findEmailBeanByEidAndBeanConstructionTest() throws SQLException {
        EmailBean foundEmail = DAO.findEmailByEid(1);
        assertEquals(ebArr[2], foundEmail);
    }

    @Test
    public void findAllReceiversLengthEqualsTest() throws SQLException {
        EmailBean[] foundReceivers = DAO.findAllReceived("receive.1532483@gmail.com");
        assertEquals(foundReceivers.length, 6);
    }

    @Test
    public void findAllSenderLengthEqualsTest() throws SQLException {
        EmailBean[] foundSender = DAO.findAllSent("send.1532483@gmail.com");
        assertEquals(foundSender.length, 7);
    }

    @Test
    public void findAllByFolderLengthEqualsTest() throws SQLException {
        EmailBean[] foundByFolder = DAO.findAllByFolder("SENT");
        assertEquals(foundByFolder.length, 7);
    }

    @Test
    public void senderEmailsAndSentFolderRelationshipTest() throws SQLException {
        EmailBean[] foundSender = DAO.findAllSent("send.1532483@gmail.com");
        EmailBean[] foundSENTByFolder = DAO.findAllByFolder("SENT");
        Assert.assertArrayEquals(foundSender, foundSENTByFolder);
    }

    @Test
    public void updateReceiveDateTest() throws SQLException {
        int eid = DAO.insertEmail(ebArr[0]);
        assertEquals(true, DAO.updateReceivedDate(eid, new Date(System.currentTimeMillis())));
    }

    @Test
    public void updateValidFolderTest() throws SQLException {
        int eid = DAO.insertEmail(ebArr[3]);
        assertEquals(true, DAO.updateEmailFolder(eid, "INBOX"));
    }

    @Test
    public void updateInvalidFolderTest() throws SQLException {
        int eid = DAO.insertEmail(ebArr[3]);
        assertEquals(false, DAO.updateEmailFolder(eid, "WOAH"));
    }

    //Happens before every test that is why each test doesn't commit to the DB
    @Before
    public void reconstructDB() {
        String data = loadAsString("CREATE_TABLES_TEST.sql");
        try (Connection connection = DriverManager.getConnection(url, user, password)) {
            for (String statement : splitStatements(new StringReader(data), ";")) {
                connection.prepareStatement(statement).execute();
            }
        } catch (SQLException sqle) {
            throw new RuntimeException("Failed seeding database", sqle);
        }
    }

    //taken from ken's example
    private String loadAsString(String path) {
        try (InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(path)) {
            Scanner scanner = new Scanner(inputStream);
            return scanner.useDelimiter("\\A").next();
        } catch (IOException e) {
            throw new RuntimeException("Unable to close input stream", e);
        }
    }

    //this method is similar to ken's code but I modified it to make it more efficient 
    //StringBuilder is not efficient
    private List<String> splitStatements(Reader reader, String statementDelimiter) {
        BufferedReader bufferedReader = new BufferedReader(reader);
        String sqlStatement = "";
        List<String> stmts = new ArrayList<>();
        try {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                line = line.trim();
                if (line.isEmpty() || isComment(line)) {
                    continue;
                }
                sqlStatement += line;
                if (line.endsWith(statementDelimiter)) {
                    stmts.add(sqlStatement);
                    sqlStatement = "";
                }
            }
            return stmts;
        } catch (IOException e) {
            throw new RuntimeException("Failed parsing sql", e);
        }
    }

    private boolean isComment(String line) {
        return line.startsWith("--");
    }

}
