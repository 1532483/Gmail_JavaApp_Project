-- Created by Vertabelo (http://vertabelo.com)
-- Last modification date: 2018-09-18 22:11:41.056

DROP DATABASE IF EXISTS JAGDBTest;
CREATE DATABASE JAGDBTest;

USE JAGDBTest;

DROP USER IF EXISTS creator@localhost;
CREATE USER creator@'localhost' IDENTIFIED WITH mysql_native_password BY '1532483sro' REQUIRE NONE;
GRANT ALL ON JAGDBTest.* TO creator@'localhost';

FLUSH PRIVILEGES;






