USE JAGDBTest;

DROP TABLE IF EXISTS ATTACHMENT;
DROP TABLE IF EXISTS RECIPIENT;
DROP TABLE IF EXISTS EMAIL;
DROP TABLE IF EXISTS FOLDER;


-- tables
-- Table: Attachments
CREATE TABLE Attachment (
    eid int NOT NULL,
    filename varchar(255) NOT NULL,
    contentId varchar(255),
    isInline bool NOT NULL
);

-- Table: Email
CREATE TABLE Email (
    eid int NOT NULL AUTO_INCREMENT,
    sender_address varchar(255) NOT NULL,
    sender_name varchar(255),
    subject varchar(255),
    text_message mediumtext NOT NULL,
    html_message mediumtext NOT NULL,
    priority int NOT NULL,
    sent_date timestamp NOT NULL,
    received_date timestamp,
    folder_id int NOT NULL,
    user_address varchar(255) NOT NULL,
    CONSTRAINT Email_pk PRIMARY KEY (eid)
);

-- Table: Folder
CREATE TABLE Folder (
    folder_id int NOT NULL AUTO_INCREMENT,
    folder_name varchar(255) NOT NULL,
    user_address varchar(255),
    CONSTRAINT Folder_pk PRIMARY KEY (folder_id)
);

-- Table: Recipient
CREATE TABLE Recipient (
    eid int NOT NULL,
    uaddress varchar(255) NOT NULL,
    uname varchar(255),
    relation varchar(255) NOT NULL
);

-- foreign keys
-- Reference: Attachments_Email (table: Attachments)
ALTER TABLE Attachment ADD CONSTRAINT Attachment_Email FOREIGN KEY Attachment_Email (eid)
    REFERENCES Email (eid) ON DELETE CASCADE;

-- Reference: Email_Folder (table: Email)
ALTER TABLE Email ADD CONSTRAINT Email_Folder FOREIGN KEY Email_Folder (folder_id)
    REFERENCES Folder (folder_id) ON DELETE CASCADE;

-- Reference: UserEmail_Email (table: UserEmail)
ALTER TABLE Recipient ADD CONSTRAINT Recipient_Email FOREIGN KEY Recipient_Email (eid)
    REFERENCES Email (eid) ON DELETE CASCADE;


INSERT INTO FOLDER (FOLDER_NAME) VALUE 
('INBOX'), ('SENT'), ('TRASH');

INSERT INTO EMAIL 
(sender_address, sender_name, subject, text_message, html_message, priority, sent_date, received_date, folder_id, user_address) VALUE 
('send.1532483@gmail.com', 'sender', 'First Case', 'This is some text', '<h1>HEY HEY HEY</h1>', -1, '2010-08-02', '2010-08-03', 2, 'send.1532483@gmail.com'),
('send.1532483@gmail.com', 'sender', 'Second Case', 'This is some text', '<h1>HEY HEY HEY</h1>', -1, '2018-06-12', '2018-08-13', 2, 'send.1532483@gmail.com'),
('send.1532483@gmail.com', 'sender', 'Third Case', 'This is some text', '<h1>HEY HEY HEY</h1>', -1, '2017-08-23', '2017-08-30', 2, 'send.1532483@gmail.com'),
('send.1532483@gmail.com', 'sender', 'Fourth Case', 'This is some text', '<h1>HEY HEY HEY</h1>', -1, '2010-08-02', '2010-08-03', 2, 'send.1532483@gmail.com'),
('send.1532483@gmail.com', 'sender', null, 'This is the fifth case', '<h1>HEY HEY HEY</h1>', -1, '2010-08-02', '2010-08-03', 2, 'send.1532483@gmail.com'),
('send.1532483@gmail.com', 'sender', 'Sixth Case', 'This is some text', '<h1>HEY HEY HEY</h1>', -1, '2010-08-02', '2010-08-03', 2, 'send.1532483@gmail.com'),
('send.1532483@gmail.com', 'sender', 'Seventh Case', 'This is some text', '<h1>HEY HEY HEY</h1>', -1, '2010-08-02', '2010-08-03', 2, 'send.1532483@gmail.com');

-- To know which email_id it relates to in the db use hashcode 
-- if recipients null in java bean, just dont put in recipient table
INSERT INTO RECIPIENT (eid, uaddress, uname, relation) VALUE 
(1, 'receive.1532483@gmail.com', 'receiver', 'TO'),
(1, 'other.1532483@gmail.com', 'other', 'CC'),
(1, 'other.1532483@gmail.com', 'other', 'BCC'),
(2, 'other.1532483@gmail.com', 'other', 'CC'),
(2, 'other.1532483@gmail.com', 'other', 'BCC'),
(3, 'receive.1532483@gmail.com', 'receiver', 'TO'),
(3, 'other.1532483@gmail.com', 'other', 'BCC'),
(4, 'other.1532483@gmail.com', 'other', 'CC'),
(4, 'receive.1532483@gmail.com', 'receiver', 'TO'),
(5, 'receive.1532483@gmail.com', 'receiver', 'TO'),
(5, 'other.1532483@gmail.com', 'other', 'CC'),
(5, 'other.1532483@gmail.com', 'other', 'BCC'),
(6, 'receive.1532483@gmail.com', 'receiver', 'TO'),
(6, 'other.1532483@gmail.com', 'other', 'CC'),
(6, 'other.1532483@gmail.com', 'other', 'BCC'),
(7, 'receive.1532483@gmail.com', 'receiver', 'TO'),
(7, 'other.1532483@gmail.com', 'other', 'CC'),
(7, 'other.1532483@gmail.com', 'other', 'BCC');

-- byte[] are not real values for testing purposes
-- temporary
INSERT INTO ATTACHMENT (eid, filename, contentId, isInline) VALUE
(6, 'davis_image.png', null, false),
(7, 'davis_image.png', '<davis_image.png>', true),
(7, 'davis_image.png', null, false);

