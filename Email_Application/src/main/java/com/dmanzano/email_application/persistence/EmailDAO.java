package com.dmanzano.email_application.persistence;

import com.dmanzano.email_application.beans.AddressBean;
import com.dmanzano.email_application.data.ConfigProperties;
import com.dmanzano.email_application.constants.RecipientType;
import static com.dmanzano.email_application.constants.RecipientType.*;
import com.dmanzano.email_application.beans.AttachmentBean;
import com.dmanzano.email_application.beans.EmailBean;
import java.io.IOException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Mail Storage Module
 *
 * @author Danny
 * @version 3.4
 */
public class EmailDAO{

    private final static Logger LOG = LoggerFactory.getLogger(EmailDAO.class);

    private final String url;
    private final String user;
    private final String password;
    private final String appUser;

    /**
     * Made for testing purposes
     * @param url
     * @param user
     * @param password
     * @param appUser
     */
    public EmailDAO(String url, String user, String password, String appUser) {
        this.url = url;
        this.user = user;
        this.password = password;
        this.appUser = appUser;
        LOG.info("EmailDAO instantiated for testing purposes");
    }
    
    /**
     * Sets up the SQL properties and instantiates the DAO
     *
     * @throws IOException
     */
    public EmailDAO() throws IOException {
        ConfigProperties cp = new ConfigProperties();
        Properties prop = cp.loadConfig();
        this.url = prop.getProperty("jdbcUrl");
        this.user = prop.getProperty("sqlUName");
        this.password = prop.getProperty("sqlUPwd");
        this.appUser = prop.getProperty("uAddress");
        LOG.info("EmailDAO instantiated and properties set\n\t" + this.url + "\n\t" + this.user + "\n\t" + this.password + "\n\t" + this.appUser);
    }

    /**
     * Looks through the DB for a specific id Can only be used if email id is
     * known
     *
     * @param eid
     * @return
     * @throws SQLException
     */
    public EmailBean findEmailByEid(int eid) throws SQLException {
        String selectQuery = "SELECT * FROM email JOIN folder ON email.folder_id = folder.folder_id WHERE email.eid = ? AND email.user_address = ?";
        try (Connection con = DriverManager.getConnection(this.url, this.user, this.password);
                PreparedStatement ps = con.prepareStatement(selectQuery)) {
            LOG.info("findEmailByEid successfully obtained the connection");
            ps.setInt(1, eid);
            ps.setString(2, this.appUser);
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    return setEmailBean(rs, con);
                }
            }
        }
        return null;
    }

    /**
     *
     * @param rs
     * @param con
     * @return
     * @throws SQLException
     */
    private EmailBean setEmailBean(ResultSet rs, Connection con) throws SQLException {

        EmailBean eb = new EmailBean();
        eb.setEid(rs.getInt("eid"));
        eb.setFolder(rs.getString("folder_name"));
        eb.setSender(new AddressBean(rs.getString("sender_name"), rs.getString("sender_address")));
        eb.setPriority(rs.getInt("priority"));
        eb.setSubject(rs.getString("subject"));
        eb.setTextMessage(rs.getString("text_message"));
        eb.setHtmlMessage(rs.getString("html_message"));
        eb.setSentDate(rs.getTimestamp("sent_date"));
        eb.setReceivedDate(rs.getTimestamp("received_date"));
        int id = rs.getInt("eid");
        eb.setAttachments(getAttachments(id, con));
        eb.setReceivers(getRecipients(id, con, TO));
        eb.setCcs(getRecipients(id, con, CC));
        eb.setBccs(getRecipients(id, con, BCC));
        LOG.info("setEmailBean: " + eb);

        return eb;
    }

    /**
     *
     * @param eid
     * @param con
     * @return
     * @throws SQLException
     */
    private AttachmentBean[] getAttachments(int eid, Connection con) throws SQLException {
        try (PreparedStatement ps = con.prepareStatement("SELECT * FROM ATTACHMENT WHERE eid = ?")) {
            ps.setInt(1, eid);
            ResultSet rs = ps.executeQuery();
            List<AttachmentBean> lab = new ArrayList<>();
            AttachmentBean ab;
            while (rs.next()) {
                ab = new AttachmentBean();
                ab.setFilename(rs.getString("filename"));
                ab.setContentId(rs.getString("contentId"));
                ab.setInline(rs.getBoolean("isInline"));
                lab.add(ab);
            }
            LOG.info("getAttachment list size: " + lab.size() + "\nEmail id: " + eid);
            return lab.toArray(new AttachmentBean[lab.size()]);
        }
    }

    /**
     *
     * @param eid
     * @param con
     * @param rt
     * @return
     * @throws SQLException
     */
    private AddressBean[] getRecipients(int eid, Connection con, RecipientType rt) throws SQLException {
        try (PreparedStatement ps = con.prepareCall("SELECT * FROM RECIPIENT WHERE eid = ? AND relation = ?")) {
            ps.setInt(1, eid);
            ps.setString(2, rt.toString());
            List<AddressBean> ls = new ArrayList<>();
            try (ResultSet rs = ps.executeQuery();) {
                String name, address;
                while (rs.next()) {
                    name = rs.getString("uname");
                    address = rs.getString("uaddress");
                    ls.add(new AddressBean(name, address));
                }
            }
            LOG.info("getAttachment list size: " + ls.size() + "\nEmail id: " + eid);

            return ls.toArray(new AddressBean[ls.size()]);

        }
    }

    /**
     * Find every email where the recipient is the one specified in the argument
     *
     * @param receiverAddress
     * @return
     * @throws SQLException
     */
    public EmailBean[] findAllReceived(String receiverAddress) throws SQLException {
        String selectAllQuery = "SELECT * FROM email JOIN folder ON email.folder_id = folder.folder_id "
                + "JOIN recipient ON recipient.eid = email.eid WHERE uaddress = ? AND email.user_address = ? GROUP BY email.eid";
        try (Connection con = DriverManager.getConnection(this.url, this.user, this.password);
                PreparedStatement ps = con.prepareStatement(selectAllQuery)) {
            LOG.info("findAllReceived successfully obtained the connection");
            ps.setString(1, receiverAddress);
            ps.setString(2, this.appUser);
            List<EmailBean> leb = new ArrayList<>();
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    leb.add(setEmailBean(rs, con));
                }
            }
            LOG.info("findEmailByEid list size: " + leb.size());
            return leb.toArray(new EmailBean[leb.size()]);
        }
    }

    /**
     * Find every email where the sender is the one specified in the argument
     *
     * @param senderAddress
     * @return
     * @throws SQLException
     */
    public EmailBean[] findAllSent(String senderAddress) throws SQLException {
        String selectAllQuery = "SELECT * FROM email JOIN folder ON email.folder_id = folder.folder_id "
                + "WHERE sender_address = ? AND email.user_address = ?";
        try (Connection con = DriverManager.getConnection(this.url, this.user, this.password);
                PreparedStatement ps = con.prepareStatement(selectAllQuery)) {
            LOG.info("findAllSent successfully obtained the connection");
            ps.setString(1, senderAddress);
            ps.setString(2, this.appUser);
            List<EmailBean> leb = new ArrayList<>();
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    leb.add(setEmailBean(rs, con));
                }
            }
            LOG.info("findAllSent list size: " + leb.size());
            return leb.toArray(new EmailBean[leb.size()]);
        }
    }

    /**
     * Find every email pertaining to a specific folder
     *
     * @param name
     * @return
     * @throws SQLException
     */
    public EmailBean[] findAllByFolder(String name) throws SQLException {
        String selectAllQuery = "SELECT * FROM email JOIN folder ON email.folder_id = folder.folder_id "
                + "WHERE folder_name = ? AND email.user_address = ?";
        try (Connection con = DriverManager.getConnection(this.url, this.user, this.password);
                PreparedStatement ps = con.prepareStatement(selectAllQuery)) {
            LOG.info("findAllByFolder successfully obtained the connection\nFolder:" + name);
            ps.setString(1, name);
            ps.setString(2, this.appUser);
            List<EmailBean> leb = new ArrayList<>();
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    leb.add(setEmailBean(rs, con));
                }
            }
            LOG.info("findAllByFolder list size: " + leb.size());
            return leb.toArray(new EmailBean[leb.size()]);
        }
    }

    /**
     * Update received date of a specific email
     *
     * @param eid
     * @param receivedDate
     * @throws SQLException
     */
    public boolean updateReceivedDate(int eid, Date receivedDate) throws SQLException {
        String updateDate = "UPDATE email SET received_date = ? WHERE eid = ?";
        try (Connection con = DriverManager.getConnection(url, user, password);
                PreparedStatement ps = con.prepareStatement(updateDate)) {
            LOG.info("updateReceivedDate successfully obtained the connection");
            ps.setDate(1, receivedDate);
            ps.setInt(2, eid);
            int rowsAffected = ps.executeUpdate();
            LOG.info("updateReceivedDate rows affected: " + rowsAffected);
            return rowsAffected == 1;
        }
    }

    /**
     * Update the folder association of a specific email
     *
     * @param eid
     * @param ftype
     * @throws SQLException
     */
    public boolean updateEmailFolder(int eid, String ftype) throws SQLException {
        String updateDate = "UPDATE email SET folder_id = ? WHERE eid = ?";
        String getFolderId = "SELECT folder_id FROM folder WHERE folder_name = ?";

        try (Connection con = DriverManager.getConnection(url, user, password);
                PreparedStatement updateStmt = con.prepareStatement(updateDate);
                PreparedStatement getStmt = con.prepareStatement(getFolderId)) {
            LOG.info("updateEmailFolder successfully obtained the connection");
            getStmt.setString(1, ftype);
            try (ResultSet getRS = getStmt.executeQuery();) {
                if (getRS.next()) {
                    updateStmt.setInt(1, getRS.getInt("folder_id"));
                    updateStmt.setInt(2, eid);
                    int rowsAffected = updateStmt.executeUpdate();
                    LOG.info("updateEmailFolder rows affected: " + rowsAffected);
                    return rowsAffected == 1;
                }
            }
            return false;
        }
    }

    /**
     * Remove the email of a specific id
     *
     * @param eid
     * @throws SQLException
     */
    public boolean removeEmail(int eid) throws SQLException {
        String removeEmail = "DELETE FROM email WHERE eid = ?";
        try (Connection con = DriverManager.getConnection(this.url, this.user, this.password);
                PreparedStatement ps = con.prepareStatement(removeEmail)) {
            LOG.info("updateEmailFolder successfully obtained the connection");
            ps.setInt(1, eid);
            int rowsAffected = ps.executeUpdate();
            LOG.info("updateEmailFolder rows affected: " + rowsAffected);
            return rowsAffected < 0;
        }
    }

    /**
     * Insert the EmailBean data to the DB
     *
     * @param email
     * @return int
     * @throws SQLException
     */
    public int insertEmail(EmailBean email) throws SQLException {

        String insertEmail = "INSERT INTO email (sender_address, subject, text_message, "
                + "html_message, priority, sent_date, received_date, folder_id, sender_name, user_address) VALUE (?,?,?,?,?,?,?,?,?,?)";
        String queryFolder = "SELECT folder_id FROM FOLDER WHERE folder_name = ?";
        int eid = -1;
        try (Connection con = DriverManager.getConnection(url, user, password);
                PreparedStatement queryFolderStmt = con.prepareCall(queryFolder);
                PreparedStatement insertEmailStmt = con.prepareCall(insertEmail)) {
            LOG.info("insertEmail successfully obtained the connection");
            queryFolderStmt.setString(1, email.getFolder());
            try (ResultSet queryFolderRS = queryFolderStmt.executeQuery();) {
                if (queryFolderRS.next()) {
                    con.setAutoCommit(false);
                    insertEmailStmt.setString(1, email.getSender().getUserAddress());
                    insertEmailStmt.setString(2, email.getSubject());
                    insertEmailStmt.setString(3, email.getTextMessage());
                    insertEmailStmt.setString(4, email.getHtmlMessage());
                    insertEmailStmt.setInt(5, email.getPriority());
                    insertEmailStmt.setTimestamp(6, email.getSentDate());
                    insertEmailStmt.setTimestamp(7, email.getReceivedDate());
                    insertEmailStmt.setInt(8, queryFolderRS.getInt("folder_id"));
                    insertEmailStmt.setString(9, email.getSender().getUserName());
                    insertEmailStmt.setString(10, this.appUser);
                    insertEmailStmt.executeUpdate();

                    try (ResultSet queryEmailRS = insertEmailStmt.getGeneratedKeys()) {
                        if (queryEmailRS.next()) {
                            eid = queryEmailRS.getInt(1);
                            email.setEid(eid);
                            insertRecipients(email, con);
                            insertAttachments(email.getAttachments(), email.getEid(), con);
                            con.commit();
                        }
                    }

                }
            }
        }
        LOG.info("insertEmail email id: " + eid);
        return eid;
    }

    /**
     *
     * @param ab
     * @param eid
     * @param con
     * @throws SQLException
     */
    private void insertAttachments(AttachmentBean[] ab, int eid, Connection con) throws SQLException {
        String insertAttachments = "INSERT INTO attachment (eid, filename, isInline, contentId) VALUE (?,?,?,?)";
        try (PreparedStatement ps = con.prepareStatement(insertAttachments)) {
            for (int i = 0; i < ab.length; i++) {
                ps.setInt(1, eid);
                ps.setString(2, ab[i].getFilename());
                //ps.setBytes(3, ab[i].getByteArray());
                //Cannot store attachment content to the database
                //Is going to be stored in the imap receive
                ps.setBoolean(3, ab[i].isInline());
                ps.setString(4, ab[i].getContentId());
                ps.executeLargeUpdate();
            }

        }

    }

    /**
     *
     * @param eb
     * @param con
     * @throws SQLException
     */
    private void insertRecipients(EmailBean eb, Connection con) throws SQLException {
        String insertRecipients = "INSERT INTO recipient (eid, uname, relation, uaddress) VALUE (?,?,?,?)";
        try (PreparedStatement ps = con.prepareStatement(insertRecipients)) {
            AddressBean[] to = eb.getReceivers();
            AddressBean[] cc = eb.getCcs();
            AddressBean[] bcc = eb.getBccs();
            for (int i = 0; i < to.length; i++) {
                ps.setInt(1, eb.getEid());
                ps.setString(2, to[i].getUserName());
                ps.setString(3, TO.toString());
                ps.setString(4, to[i].getUserAddress());
                ps.executeUpdate();
            }

            for (int i = 0; i < cc.length; i++) {
                ps.setInt(1, eb.getEid());
                ps.setString(2, cc[i].getUserName());
                ps.setString(3, CC.toString());
                ps.setString(4, cc[i].getUserAddress());
                ps.executeUpdate();
            }

            for (int i = 0; i < bcc.length; i++) {
                ps.setInt(1, eb.getEid());
                ps.setString(2, bcc[i].getUserName());
                ps.setString(3, BCC.toString());
                ps.setString(4, bcc[i].getUserAddress());
                ps.executeUpdate();
            }
        }

    }

}
