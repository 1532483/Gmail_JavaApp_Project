package com.dmanzano.email_application.persistence;

import com.dmanzano.email_application.data.ConfigProperties;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Folder Storage Module
 *
 * @author Danny
 * @version 2.5
 */
public class FolderDAO {

    private final static Logger LOG = LoggerFactory.getLogger(EmailDAO.class);

    private final String url;
    private final String user;
    private final String password;
    private final String appUser;

    /**
     * Made for testing purposes
     * @param url
     * @param user
     * @param password
     * @param appUser
     */
    public FolderDAO(String url, String user, String password, String appUser){
        this.url = url;
        this.user = user;
        this.password = password;
        this.appUser = appUser;
    }
    
    /**
     * Sets up the SQL properties and instantiates the DAO
     *
     * @throws IOException
     */
    public FolderDAO() throws IOException {
        ConfigProperties cp = new ConfigProperties();
        Properties prop = cp.loadConfig();
        this.url = prop.getProperty("jdbcUrl");
        this.user = prop.getProperty("sqlUName");
        this.password = prop.getProperty("sqlUPwd");
        this.appUser = prop.getProperty("uAddress");
        LOG.info("FolderDAO instantiated and properties set.\n\t" + this.url + "\n\t" + this.user + "\n\t" + this.password + "\n\t" + this.appUser);
    }

    /**
     * Return list of all folders in DB
     *
     * @return string[]
     * @throws SQLException
     */
    public String[] getFolders() throws SQLException {
        String getDefaultFolder = "SELECT folder_name FROM folder WHERE user_address IS NULL OR user_address = ?";
        List<String> lstr = new ArrayList<>();
        try (Connection con = DriverManager.getConnection(url, user, password);
                PreparedStatement ps = con.prepareStatement(getDefaultFolder)) {
            ps.setString(1, this.appUser);
            try (ResultSet rs = ps.executeQuery()) {
                LOG.info("getFolders successfully obtained the connection");
                while (rs.next()) {
                    lstr.add(rs.getString("folder_name"));
                } 
                LOG.info("getFolders list size: " + lstr.size());
                return lstr.toArray(new String[lstr.size()]);
            }
        }
    }

    /**
     * Insert a folder entry according to parameter string
     *
     * @param folderName
     * @return boolean
     * @throws SQLException
     */
    public boolean addFolder(String folderName) throws SQLException {
        String addFolder = "INSERT INTO folder (folder_name, user_address) VALUES (?, ?)";
        try (Connection con = DriverManager.getConnection(url, user, password);
                PreparedStatement ps = con.prepareStatement(addFolder)) {
            LOG.info("addFolder successfully obtained the connection");
            ps.setString(1, folderName);
            ps.setString(2, this.appUser);
            int rowsAffected = ps.executeUpdate();
            LOG.info("addFolder rows affected: " + rowsAffected);
            return rowsAffected > 0;
        }
    }

    /**
     * Remove folder entry according to parameter string
     *
     * @param folderName
     * @return boolean
     * @throws SQLException
     */
    public boolean removeFolder(String folderName) throws SQLException {
        String removeFolder = "DELETE FROM folder WHERE folder_name = ?";
        try (Connection con = DriverManager.getConnection(url, user, password);
                PreparedStatement ps = con.prepareStatement(removeFolder)) {
            LOG.info("removeFolder successfully obtained the connection");
            ps.setString(1, folderName);
            int rowsAffected = ps.executeUpdate();
            LOG.info("removeFolder rows affected: " + rowsAffected);
            return rowsAffected > 0;
        }
    }
}
