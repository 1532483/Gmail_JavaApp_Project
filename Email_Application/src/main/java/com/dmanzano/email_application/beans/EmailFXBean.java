package com.dmanzano.email_application.beans;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ListProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author Danny
 * @version 3.0
 */
public class EmailFXBean {
    private ObjectProperty<AddressFXBean> sender;
    private ListProperty<AddressFXBean> receivers;
    private ListProperty<AddressFXBean> ccs;
    private ListProperty<AddressFXBean> bccs;
    private StringProperty subject;
    private StringProperty textMessage;
    private StringProperty htmlMessage;
    private ListProperty<AttachmentFXBean> attachments;
    private IntegerProperty priority;
    private ObjectProperty<Timestamp> sentDate;
    private ObjectProperty<Timestamp> receivedDate;
    private IntegerProperty eid;
    private StringProperty folder;
    
    /**
     * Default Constructor
     */
    public EmailFXBean(){
        this(new EmailBean());
    }
    
    /**
     * Constructor setting params with arrays
     * 
     * @param sender
     * @param receivers
     * @param ccs
     * @param bccs
     * @param subject
     * @param textMessage
     * @param htmlMessage
     * @param attachments
     * @param priority
     * @param sentDate
     * @param receiveDate
     * @param folder 
     */
    public EmailFXBean(AddressFXBean sender, AddressFXBean[] receivers,
            AddressFXBean[] ccs, AddressFXBean[] bccs, String subject,
            String textMessage, String htmlMessage, AttachmentFXBean[] attachments, 
            int priority, Timestamp sentDate, Timestamp receiveDate, String folder){
        
        this.sender = new SimpleObjectProperty(sender);
        
        //Parsing receiver list
        List<AddressFXBean> receiverLs = new ArrayList<>();
        for(int i=0; i<receivers.length; i++){
            receiverLs.add(receivers[i]);
        }
        this.receivers = new SimpleListProperty<AddressFXBean>();
        this.receivers.set(FXCollections.observableArrayList(receiverLs));
        
        //Parsing ccs list
        List<AddressFXBean> ccsLs = new ArrayList<>();
        for(int i=0; i<ccs.length; i++){
            ccsLs.add(ccs[i]);
        }
        this.ccs = new SimpleListProperty<AddressFXBean>();
        this.ccs.set(FXCollections.observableArrayList(ccsLs));
        
        //Parsing bccs list
        List<AddressFXBean> bccsLs = new ArrayList<>();
        for(int i=0; i<bccs.length; i++){
            bccsLs.add(bccs[i]);
        }
        this.bccs = new SimpleListProperty<AddressFXBean>();
        this.bccs.set(FXCollections.observableArrayList(bccsLs));
        
        this.subject = new SimpleStringProperty(subject);
        this.textMessage = new SimpleStringProperty(textMessage);
        this.htmlMessage = new SimpleStringProperty(htmlMessage);
        
        //Parsing attachment list
        List<AttachmentFXBean> attachmentLs = new ArrayList<>();
        for(int i=0; i<attachments.length; i++){
            attachmentLs.add(attachments[i]);
        }
        this.attachments = new SimpleListProperty<AttachmentFXBean>();
        this.attachments.set(FXCollections.observableArrayList(attachmentLs));
        
        this.priority = new SimpleIntegerProperty(priority);
        this.sentDate = new SimpleObjectProperty<Timestamp>(sentDate);
        this.receivedDate = new SimpleObjectProperty<Timestamp>(receiveDate);
        this.folder = new SimpleStringProperty(folder);
        this.eid = new SimpleIntegerProperty();
        
    }
    
    /**
     * Constructor setting params with Lists
     * 
     * @param sender
     * @param receivers
     * @param ccs
     * @param bccs
     * @param subject
     * @param textMessage
     * @param htmlMessage
     * @param attachments
     * @param priority
     * @param sentDate
     * @param receiveDate
     * @param folder 
     */
    public EmailFXBean(AddressFXBean sender, List<AddressFXBean> receivers,
            List<AddressFXBean> ccs, List<AddressFXBean> bccs, String subject,
            String textMessage, String htmlMessage, List<AttachmentFXBean> attachments,
            int priority, Timestamp sentDate, Timestamp receiveDate, String folder){
        
        this.sender = new SimpleObjectProperty(sender);
        
        //Parsing receiver list
        this.receivers = new SimpleListProperty<AddressFXBean>();
        this.receivers.set(FXCollections.observableArrayList(receivers));
        
        //Parsing ccs list
        this.ccs = new SimpleListProperty<AddressFXBean>();
        this.ccs.set(FXCollections.observableArrayList(ccs));
        
        //Parsing bccs list
        this.bccs = new SimpleListProperty<AddressFXBean>();
        this.bccs.set(FXCollections.observableArrayList(bccs));
        
        //Parsing attachment list
        this.attachments = new SimpleListProperty<AttachmentFXBean>();
        this.attachments.set(FXCollections.observableArrayList(attachments));
        
        
        this.subject = new SimpleStringProperty(subject);
        this.textMessage = new SimpleStringProperty(textMessage);
        this.htmlMessage = new SimpleStringProperty(htmlMessage);
        this.priority = new SimpleIntegerProperty(priority);
        this.sentDate = new SimpleObjectProperty<Timestamp>(sentDate);
        this.receivedDate = new SimpleObjectProperty<Timestamp>(receiveDate);
        this.folder = new SimpleStringProperty(folder);
        this.eid = new SimpleIntegerProperty();
    }
    
    /**
     * Constructor setting params with EmailBean
     * @param eb 
     */
    public EmailFXBean(EmailBean eb){
        
        this.sender = new SimpleObjectProperty(new AddressFXBean(eb.getSender().getUserName(), eb.getSender().getUserAddress()));
        
        //Parsing receiver list
        List<AddressFXBean> receiverLs = new ArrayList<>();
        AddressBean[] re = eb.getReceivers() != null ? eb.getReceivers(): new AddressBean[0]; 
        AddressFXBean abRe;
        for(int i=0; i<re.length; i++){
            abRe = new AddressFXBean();
            abRe.setUserAddress(re[i].getUserAddress());
            abRe.setUserName(re[i].getUserName());
            receiverLs.add(abRe);
        }
        this.receivers = new SimpleListProperty<AddressFXBean>();
        this.receivers.set(FXCollections.observableArrayList(receiverLs));
        
        //Parsing ccs list
        List<AddressFXBean> ccsLs = new ArrayList<>();
        AddressBean[] cc = eb.getCcs() != null ? eb.getCcs() : new AddressBean[0];
        AddressFXBean abCc;
        for(int i=0; i<cc.length; i++){
            abCc = new AddressFXBean();
            abCc.setUserAddress(cc[i].getUserAddress());
            abCc.setUserName(cc[i].getUserName());
            ccsLs.add(abCc);
        }
        this.ccs = new SimpleListProperty<AddressFXBean>();
        this.ccs.set(FXCollections.observableArrayList(ccsLs));
        
        //Parsing bccs list
        List<AddressFXBean> bccsLs = new ArrayList<>();
        AddressBean[] bc = eb.getBccs() != null ? eb.getBccs() : new AddressBean[0];
        AddressFXBean abBc;
        for(int i=0; i<bc.length; i++){
            abBc = new AddressFXBean();
            abBc.setUserAddress(bc[i].getUserAddress());
            abBc.setUserName(bc[i].getUserName());
            bccsLs.add(abBc);
        }
        this.bccs = new SimpleListProperty<AddressFXBean>();
        this.bccs.set(FXCollections.observableArrayList(bccsLs));
        
        this.subject = new SimpleStringProperty(eb.getSubject());
        this.textMessage = new SimpleStringProperty(eb.getTextMessage());
        this.htmlMessage = new SimpleStringProperty(eb.getHtmlMessage());
        
        //Parsing attachment list
        List<AttachmentFXBean> attachmentLs = new ArrayList<>();
        AttachmentBean[] a = eb.getAttachments() != null ? eb.getAttachments() : new AttachmentBean[0];
        AttachmentFXBean abA;
        for(int i=0; i<a.length; i++){
            abA = new AttachmentFXBean();
            abA.setContent(a[i].getByteArray());
            abA.setFilename(a[i].getFilename());
            abA.setInline(a[i].isInline());
            abA.setContentId(a[i].getContentId());
            attachmentLs.add(abA);
        }
        this.attachments = new SimpleListProperty<AttachmentFXBean>();
        this.attachments.set(FXCollections.observableArrayList(attachmentLs));
        
        this.priority = new SimpleIntegerProperty(eb.getPriority());
        this.sentDate = new SimpleObjectProperty<Timestamp>(eb.getSentDate());
        this.receivedDate = new SimpleObjectProperty<Timestamp>(eb.getReceivedDate());
        this.folder = new SimpleStringProperty(eb.getFolder());
        this.eid = new SimpleIntegerProperty(eb.getEid());
        
    }

    /**
     * 
     * @param sender 
     */
    public void setSender(AddressFXBean sender) {
        this.sender.setValue(sender);
    }
    
    /**
     * 
     * @return ObjectProperty<AddressFXBean>
     */
    public ObjectProperty<AddressFXBean> senderProperty(){
        return sender;
    }
    
    /**
     * 
     * @return AddressFXBean 
     */
    public AddressFXBean getSender() {
        return sender.getValue();
    }

    /**
     * 
     * @param receivers 
     */
    public void setReceivers(ObservableList<AddressFXBean> receivers) {
        this.receivers.setValue(receivers);
    }
    
    /**
     * 
     * @return ListProperty<AddressFXBean>
     */
    public ListProperty<AddressFXBean> receiversProperty() {
        return receivers;
    }
    
    /**
     * 
     * @return ObservableList<AddressFXBean>
     */
    public ObservableList<AddressFXBean> getReceivers(){
        return receivers.getValue();
    } 

    /**
     * 
     * @param ccs 
     */
    public void setCcs(ObservableList<AddressFXBean> ccs) {
        this.ccs.setValue(ccs);
    }
    
    /**
     * 
     * @return ListProperty<AddressFXBean> 
     */
    public ListProperty<AddressFXBean> ccsProperty() {
        return ccs;
    }
    
    /**
     * 
     * @return ObservableList<AddressFXBean>
     */
    public ObservableList<AddressFXBean> getCcs(){
        return this.ccs.getValue();
    }

    /**
     * 
     * @param bccs 
     */
    public void setBccs(ObservableList<AddressFXBean> bccs) {
        this.bccs.setValue(bccs);
    }
    
    /**
     * 
     * @return ListProperty<AddressFXBean>
     */
    public ListProperty<AddressFXBean> bccsProperty() {
        return bccs;
    }
    
    /**
     * 
     * @return ObservableList<AddressFXBean>
     */
    public ObservableList<AddressFXBean> getBccs(){
        return this.bccs.getValue();
    }
        
    /**
     * 
     * @param subject 
     */
    public void setSubject(String subject) {
        this.subject.setValue(subject);
    }
    
    /**
     * 
     * @return StringProperty 
     */
    public StringProperty subjectProperty(){
        return this.subject;
    }
    
    /**
     * 
     * @return String
     */
    public String getSubject() {
        return this.subject.getValue();
    }

    /**
     * 
     * @param textMessage 
     */
    public void setTextMessage(String textMessage) {
        this.textMessage.setValue(textMessage);
    }
    
    /**
     * 
     * @return StringProperty 
     */
    public StringProperty textMessageProperty(){
        return this.textMessage;
    }
    
    /**
     * 
     * @return String
     */
    public String getTextMessage() {
        return textMessage.getValue();
    }
    
    /**
     * 
     * @param htmlMessage 
     */
    public void setHtmlMessage(String htmlMessage) {
        this.htmlMessage.setValue(htmlMessage);
    }
    
    /**
     * 
     * @return StringProperty
     */
    public StringProperty htmlMessageProperty() {
        return htmlMessage;
    }
    
    /**
     * 
     * @return String
     */
    public String getHtmlMessage(){
        return this.htmlMessage.getValue();
    }

    /**
     * 
     * @param attachments 
     */
    public void setAttachments(ObservableList<AttachmentFXBean> attachments) {
        this.attachments.setValue(attachments);
    }
    
    /**
     * 
     * @return ListProperty<AttachmentFXBean>
     */
    public ListProperty<AttachmentFXBean> attachmentsProperty() {
        return attachments;
    }
    
    /**
     * 
     * @return ObservableList<AttachmentFXBean>
     */
    public ObservableList<AttachmentFXBean> getAttachments(){
        return this.attachments.getValue();
    }

    /**
     * 
     * @param priority 
     */
    public void setPriority(int priority) {
        this.priority.setValue(priority);
    }

    /**
     * 
     * @return IntegerProperty
     */
    public IntegerProperty priorityProperty() {
        return priority;
    }
    
    /**
     * 
     * @return int
     */
    public int getPriority(){
        return this.priority.getValue();
    }

    /**
     * 
     * @param sentDate 
     */
    public void setSentDate(Timestamp sentDate) {
        this.sentDate.setValue(sentDate);
    }

    /**
     * 
     * @return ObjectProperty<Timestamp>
     */
    public ObjectProperty<Timestamp> sentDateProperty() {
        return sentDate;
    }
    
    /**
     * 
     * @return Timestamp 
     */
    public Timestamp getSentDate(){
        return this.sentDate.getValue();
    }

    /**
     * 
     * @param receivedDate 
     */
    public void setReceivedDate(Timestamp receivedDate) {
        this.receivedDate.setValue(receivedDate);
    }

    /**
     * 
     * @return ObjectProperty<Timestamp> 
     */
    public ObjectProperty<Timestamp> receivedDateProperty() {
        return receivedDate;
    }
    
    /**
     * 
     * @return Timestamp
     */
    public Timestamp getReceivedDate(){
        return this.receivedDate.getValue();
    }

    /**
     * 
     * @param eid 
     */
    public void setEid(int eid) {
        this.eid.setValue(eid);
    }
    
    /**
     * 
     * @return IntegerProperty 
     */
    public IntegerProperty eidProperty() {
        return eid;
    }
    
    /**
     * 
     * @return int 
     */
    public int getEid(){
        return eid.getValue();
    }

    /**
     * 
     * @param folder 
     */
    public void setFolder(String folder) {
        this.folder.setValue(folder);
    }

    /**
     * 
     * @return StringProperty 
     */
    public StringProperty folderProperty() {
        return folder;
    }
    
    /**
     * 
     * @return String
     */
    public String getFolder(){
        return folder.getValue();
    }
}
