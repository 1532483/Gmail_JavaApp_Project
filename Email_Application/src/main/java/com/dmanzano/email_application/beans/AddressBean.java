package com.dmanzano.email_application.beans;

import java.io.Serializable;
import java.util.Objects;
import jodd.mail.EmailAddress;

/**
 *
 * @author Danny
 * @version 3.3
 */
public final class AddressBean implements Serializable{
    private String userName;
    private String userAddress;

    
    /**
     * Sets private fields to default
     */
    public AddressBean(){
        this("","");
    }
    
    /**
     * Receives and stores user name and address
     * @param userName
     * @param userAddress 
     */
    public AddressBean(String userName, String userAddress){
        setUserName(userName);
        setUserAddress(userAddress);
    }
    
    /**
     * 
     * @return String
     */
    public String getUserName() {
        return userName;
    }

    /**
     * 
     * @param userName 
     */
    public void setUserName(String userName) {
            this.userName = userName;
    }

    /**
     * 
     * @return String
     */
    public String getUserAddress() {
        return userAddress;
    }

    /**
     * 
     * @param userAddress 
     */
    public void setUserAddress(String userAddress) {
            this.userAddress = userAddress;
    }
    
    /**
     * Return EmailAddress format
     * 
     * @return EmailAddress
     */
    public EmailAddress getEmailAddress(){
        return new EmailAddress(this.userName, this.userAddress);
    }

    /**
     * Format string to represent all important properties
     * 
     * @return String
     */
    @Override
    public String toString() {
        return "\nAddressBean{" + "\n\tuserName=" + userName + ", \n\tuserAddress=" +
                userAddress + "\n}";
    }

    /**
     * Uses same properties as .equals()
     * 
     * @return int
     */
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + Objects.hashCode(this.userAddress);
        return hash;
    }

    /**
     * Uses: userAddress
     * To determine equality
     * 
     * @param obj
     * @return boolean
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AddressBean other = (AddressBean) obj;
        return Objects.equals(this.userAddress, other.userAddress);
    }
}
