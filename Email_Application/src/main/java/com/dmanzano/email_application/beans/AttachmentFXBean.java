package com.dmanzano.email_application.beans;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author Danny
 * @version 3.0
 */
public class AttachmentFXBean {
    private BooleanProperty inline;
    private StringProperty filename;
    private ObjectProperty<byte[]> content;
    private StringProperty contentId;
    
    /**
     * Default Constructor
     */
    public AttachmentFXBean(){
        this("", new byte[0], false, "");
    }
    
    /**
     * Constructor initializing the properties
     * 
     * @param filename
     * @param byteArray
     * @param inline 
     * @param cid
     */
    public AttachmentFXBean(String filename, byte[] byteArray, boolean inline, String cid){
        this.inline = new SimpleBooleanProperty(inline);
        this.filename = new SimpleStringProperty(filename);
        this.content = new SimpleObjectProperty<>(byteArray);
        this.contentId = new SimpleStringProperty(cid);
    }

    /**
     * 
     * @return boolean
     */
    public boolean getInline() {
        return inline.getValue();
    }

    /**
     * 
     * @param isInline 
     */
    public void setInline(boolean isInline) {
        this.inline.setValue(isInline);
    }
    
    /**
     * 
     * @return BooleanProperty 
     */
    public BooleanProperty inlineProperty(){
        return this.inline;
    }

    /**
     * 
     * @return String 
     */
    public String getFilename() {
        return filename.getValue();
    }

    /**
     * 
     * @param filename 
     */
    public void setFilename(String filename) {
        this.filename.setValue(filename);
    }
    
    /**
     * 
     * @return StringProperty
     */
    public StringProperty filenameProperty(){
        return this.filename;
    }

    /**
     * 
     * @return byte[] 
     */
    public byte[] getContent() {
        return content.getValue();
    }

    /**
     * 
     * @param content 
     */
    public void setContent(byte[] content) {
        this.content.setValue(content);
    }
    
    /**
     * 
     * @return ObjectProperty<byte[]> 
     */
    public ObjectProperty<byte[]> contentProperty(){
        return this.content;
    }
    
    /**
     * 
     * @return String 
     */
    public String getContentId(){
        return this.contentId.getValue();
    }
    
    /**
     * 
     * @param cid 
     */
    public void setContentId(String cid){
        this.contentId.set(cid);
    }
    
    /**
     * 
     * @return StringProperty 
     */
    public StringProperty contentIdProperty(){
        return this.contentId;
    }
    
}


