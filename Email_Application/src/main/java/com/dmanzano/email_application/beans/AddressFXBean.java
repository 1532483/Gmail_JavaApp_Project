package com.dmanzano.email_application.beans;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author Danny
 * @version 3.0
 */
public class AddressFXBean {
    private StringProperty userName;
    private StringProperty userAddress;
    
    /**
     * Default Constructor
     */
    public AddressFXBean(){
        this("", "");
    }
    
    /**
     * Set parameters
     * 
     * @param userName
     * @param userAddress 
     */
    public AddressFXBean(String userName, String userAddress){
        this.userName = new SimpleStringProperty(userName);
        this.userAddress = new SimpleStringProperty(userAddress);
    }
    
    /**
     * 
     * @return String 
     */
    public String getUserName(){
        return this.userName.getValue();
    }
    
    /**
     * 
     * @param userName 
     */
    public void setUserName(String userName){
        this.userName.setValue(userName);
    }
    
    /**
     * 
     * @return StringProperty
     */
    public StringProperty userNameProperty(){
        return this.userName;
    }
    
    /**
     * 
     * @return String
     */
    public String getUserAddress(){
        return this.userAddress.getValue();
    }
    
    /**
     * 
     * @param userAddress 
     */
    public void setUserAddress(String userAddress){
        this.userAddress.setValue(userAddress);
    } 
    
    /**
     * 
     * @return StringProperty
     */
    public StringProperty userAddressProperty(){
        return this.userAddress;
    }
}
