package com.dmanzano.email_application.beans;

import java.io.Serializable;
import java.util.Arrays;
import java.sql.Timestamp;
import java.util.Objects;
import jodd.mail.EmailAddress;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * Utility class to set different types of email Will subsequently write the
 * database
 *
 * @author Danny
 * @version 3.3
 */
public final class EmailBean implements Serializable {

    private final static Logger LOG = LoggerFactory.getLogger(EmailBean.class);

    private AddressBean sender;
    private AddressBean[] receivers;
    private AddressBean[] ccs;
    private AddressBean[] bccs;
    private String subject;
    private String textMessage;
    private String htmlMessage;
    private AttachmentBean[] attachments;
    private int priority;
    private Timestamp sentDate;
    private Timestamp receivedDate;
    private int eid;
    private String folder;


    /**
     * Receives and stores email informations.
     *
     * @param sender
     * @param receivers
     * @param ccs
     * @param bccs
     * @param subject
     * @param textMessage
     * @param htmlMessage
     * @param attachments
     */
    public EmailBean(AddressBean sender, AddressBean[] receivers,
            AddressBean[] ccs, AddressBean[] bccs, String subject,
            String textMessage, String htmlMessage, AttachmentBean[] attachments) {
        this(sender, receivers, ccs, bccs, subject, textMessage, htmlMessage, attachments, -1, null, null, "");
        setEid(-1);
        LOG.info("Email Bean Constructor instantiated all instance variables except"
                + "for attachedEmail, priority, and dates which are set to default");
    }

    /**
     * Receives and stores all email informations.
     *
     * @param sender
     * @param receivers
     * @param ccs
     * @param bccs
     * @param subject
     * @param textMessage
     * @param htmlMessage
     * @param attachments
     * @param priority
     * @param sentDate
     * @param folder
     * @param receiveDate
     */
    public EmailBean(AddressBean sender, AddressBean[] receivers,
            AddressBean[] ccs, AddressBean[] bccs, String subject,
            String textMessage, String htmlMessage, AttachmentBean[] attachments,
            int priority, Timestamp sentDate, Timestamp receiveDate, String folder) {
        setSender(sender);
        setReceivers(receivers);
        setCcs(ccs);
        setBccs(bccs);
        setSubject(subject);
        setTextMessage(textMessage);
        setHtmlMessage(htmlMessage);
        setAttachments(attachments);
        setPriority(priority);
        setSentDate(sentDate);
        setReceivedDate(receiveDate);
        setFolder(folder);
        setEid(-1);
        LOG.info("Email Bean Constructor instantiated all instance variables");
    }

    /**
     * Set every variables to default
     */
    public EmailBean() {
        this(null, null, null, null, "", "", "", null, -1, null, null, "");
        setEid(-1);
        LOG.info("Email Bean Constructor instantiated all instance variables to default");
    }

    /**
     *
     * @return int
     */
    public int getPriority() {
        return priority;
    }

    /**
     *
     * @param priority
     */
    public void setPriority(int priority) {
        this.priority = priority;
    }

    /**
     *
     * @return Timestamp
     */
    public Timestamp getSentDate() {
        return sentDate;
    }

    /**
     *
     * @param sentDate
     */
    public void setSentDate(Timestamp sentDate) {
        this.sentDate = sentDate;
    }

    /**
     *
     * @return Timestamp
     */
    public Timestamp getReceivedDate() {
        return receivedDate;
    }

    /**
     *
     * @param receivedDate
     */
    public void setReceivedDate(Timestamp receivedDate) {
        this.receivedDate = receivedDate;
    }

    /**
     * @return AddressBean
     */
    public AddressBean getSender() {
        return sender;
    }

    /**
     * @param sender 
     */
    public void setSender(AddressBean sender) {

        this.sender = sender;
    }

    /**
     * @return AddressBean[]
     */
    public AddressBean[] getReceivers() {
        return receivers;
    }
    
    /**
     * Format to EmailAddress[]
     * 
     * @return EmailAddress[]
     */
    public EmailAddress[] getReceiversEmailAddress(){
        EmailAddress[] eaArr = new EmailAddress[this.receivers.length];
        for(int i=0; i<eaArr.length; i++){
            eaArr[i] = this.receivers[i].getEmailAddress();
        }
        return eaArr;
    }

    /**
     *
     * @param receivers 
     */
    public void setReceivers(AddressBean[] receivers) {
        if (receivers == null) {
            this.receivers = new AddressBean[0];
        } else {
            this.receivers = receivers;
        }
    }

    /**
     * @return AddressBean[]
     */
    public AddressBean[] getCcs() {
        return ccs;
    }
    
    /**
     * Format to EmailAddress[]
     * 
     * @return EmailAddress[]
     */
    public EmailAddress[] getCcsEmailAddress(){
        EmailAddress[] eaArr = new EmailAddress[this.ccs.length];
        for(int i=0; i<eaArr.length; i++){
            eaArr[i] = this.ccs[i].getEmailAddress();
        }
        return eaArr;
    }
    
    /**
     *
     * @param ccs 
     */
    public void setCcs(AddressBean[] ccs) {
        if (ccs == null) {
            this.ccs = new AddressBean[0];
        } else {
            this.ccs = ccs;
        }
    }

    /**
     *
     * @return AddressBean[]
     */
    public AddressBean[] getBccs() {
        return bccs;
    }
    
    /**
     * Format to EmailAddress[]
     * @return EmailAddress[]
     */
    public EmailAddress[] getBccsEmailAddress(){
        EmailAddress[] eaArr = new EmailAddress[this.bccs.length];
        for(int i=0; i<eaArr.length; i++){
            eaArr[i] = this.bccs[i].getEmailAddress();
        }
        return eaArr;
    }

    /**
     *
     * @param bccs
     */
    public void setBccs(AddressBean[] bccs) {
        if (bccs == null) {
            this.bccs = new AddressBean[0];
        } else {
            this.bccs = bccs;
        }
    }

    /**
     * @return String
     */
    public String getSubject() {
        return subject;
    }

    /**
     * @param subject
     */
    public void setSubject(String subject) {
        this.subject = subject;
    }

    /**
     * @return String
     */
    public String getTextMessage() {
        return textMessage;
    }

    /**
     * @param textMessage
     */
    public void setTextMessage(String textMessage) {
        this.textMessage = textMessage;
    }

    /**
     * @return String
     */
    public String getHtmlMessage() {
        return htmlMessage;
    }

    /**
     * @param htmlMessage
     */
    public void setHtmlMessage(String htmlMessage) {
        this.htmlMessage = htmlMessage;
    }

    /**
     * 
     * @return AttachmentBean[]
     */
    public AttachmentBean[] getAttachments() {
        return attachments;
    }

    /**
     * 
     * @param attachments 
     */
    public void setAttachments(AttachmentBean[] attachments) {
        this.attachments = attachments;
    }

    /**
     * 
     * @return int
     */
    public int getEid() {
        return eid;
    }

    /**
     * 
     * @param eid 
     */
    public void setEid(int eid) {
        this.eid = eid;
    }

    /**
     * 
     * @return String 
     */
    public String getFolder() {
        return folder;
    }

    /**
     * 
     * @param folder 
     */
    public void setFolder(String folder) {
        this.folder = folder;
    }  

    /**
     * Format to a string showing all important attributes
     * 
     * @return String 
     */
    @Override
    public String toString() {
        
        String receiversStr = "";
        for(int i=0; i<this.receivers.length; i++){
            receiversStr += this.receivers[i];
        }
        
        String ccs = "";
        for(int i=0; i<this.ccs.length; i++){
            ccs += this.ccs[i];
        }
        
        String bccs = "";
        for(int i=0; i<this.bccs.length; i++){
            bccs += this.bccs[i];
        }
        
        String attachments = "";
        for(int i=0; i<this.attachments.length; i++){
            attachments += this.attachments[i];
        }
        
        
        return "EmailBean{" + "\nsender=" + sender + ", \nreceivers=" + receiversStr + 
                ", \nccs=" + ccs + ", \nbccs=" + bccs + ", \nsubject=" + subject + 
                ", \ntextMessage=" + textMessage + ", \nhtmlMessage=" + htmlMessage + 
                ", \nattachments=" + attachments + ", \npriority=" + priority + ", \nsentDate=" + 
                sentDate + ", \nreceivedDate=" + receivedDate + ", \neid=" + eid + ", \nfolder=" + 
                folder + "\n}";
    }

    /**
     * Uses same properties as .equals()
     * 
     * @return int
     */
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 37 * hash + Objects.hashCode(this.sender);
        hash = 37 * hash + Arrays.deepHashCode(this.receivers);
        hash = 37 * hash + Arrays.deepHashCode(this.ccs);
        hash = 37 * hash + Objects.hashCode(this.subject);
        hash = 37 * hash + Objects.hashCode(this.textMessage);
        hash = 37 * hash + Objects.hashCode(this.htmlMessage);
        hash = 37 * hash + Objects.hashCode(this.attachments);
        hash = 37 * hash + this.priority;
        return hash;
    }

    /**
     * Uses: priority, subject, textMessage, htmlMessage, sender, receivers, ccs, attachments
     * To determine equality
     * 
     * @param obj
     * @return boolean
     */
    @Override
    public boolean equals(Object obj) {
        LOG.info("Email Equals()");
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final EmailBean other = (EmailBean) obj;
        LOG.info("This email instance: " + this + "\nOther email instance: " + other);
        LOG.info("Priority: " + (this.priority == other.priority));
        if (this.priority != other.priority) {
            return false;
        }
        LOG.info("Subject: " + (Objects.equals(this.subject, other.subject)));
        if (!Objects.equals(this.subject, other.subject)) {
            return false;
        }
        LOG.info("TextMessage: " + (Objects.equals(this.textMessage, other.textMessage)));
        if (!Objects.equals(this.textMessage, other.textMessage)) {
            return false;
        }
        LOG.info("HtmlMessage: " + (Objects.equals(this.htmlMessage, other.htmlMessage)));
        if (!Objects.equals(this.htmlMessage, other.htmlMessage)) {
            return false;
        }
        LOG.info("Sender: " + (Objects.equals(this.sender, other.sender)));
        if (!Objects.equals(this.sender, other.sender)) {
            return false;
        }
        LOG.info("Receivers: " + (Arrays.deepEquals(this.receivers, other.receivers)));
        if (!Arrays.deepEquals(this.receivers, other.receivers)) {
            return false;
        }
        LOG.info("Ccs: " + (Arrays.deepEquals(this.ccs, other.ccs)));
        if (!Arrays.deepEquals(this.ccs, other.ccs)) {
            return false;
        }
        LOG.info("Attachments: " + (Arrays.deepEquals(this.attachments, other.attachments)));
        return Arrays.deepEquals(this.attachments, other.attachments);
    }

    

    

}
