package com.dmanzano.email_application.beans;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Objects;

/**
 * 
 * @author Danny
 * @version 3.3
 */
public final class AttachmentBean implements Serializable{
    private boolean inline;
    private String filename;
    private byte[] byteArray;
    private String contentId;
    
    /**
     * Sets private fields to default
     * 
     */
    public AttachmentBean(){
        this("", null, false, null);
    }
    
    /**
     * Sets all properties to according parameter
     * 
     * @param filename
     * @param byteArray
     * @param inline 
     * @param cid
     */
    public AttachmentBean(String filename, byte[] byteArray, boolean inline, String cid){
        setInline(inline);
        setFilename(filename);
        setByteArray(byteArray);
        setContentId(cid);
    }

    /**
     * 
     * @return boolean
     */
    public boolean isInline() {
        return inline;
    }

    /**
     * 
     * @param isInline 
     */
    public void setInline(boolean isInline) {
        this.inline = isInline;
    }

    /**
     * 
     * @return String
     */
    public String getFilename() {
        return filename;
    }

    /**
     * 
     * @param filename 
     */
    public void setFilename(String filename) {
            this.filename = filename;
    }

    /**
     * 
     * @return byte[]
     */
    public byte[] getByteArray() {
        return byteArray;
    }

    /**
     * 
     * @param byteArray 
     */
    public void setByteArray(byte[] byteArray) {
            this.byteArray = byteArray;
    }

    /**
     * 
     * @return String 
     */
    public String getContentId() {
        return contentId;
    }

    /**
     * 
     * @param ContentId 
     */
    public void setContentId(String ContentId) {
        this.contentId = ContentId;
    }

    
    
    /**
     * Uses same properties as .equals()
     * 
     * @return int
     */
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + (this.inline ? 1 : 0);
        hash = 29 * hash + Objects.hashCode(this.filename);
        return hash;
    }

    /**
     * Uses: inline, filename, byteArray 
     * To determine equality
     * 
     * @param obj
     * @return 
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AttachmentBean other = (AttachmentBean) obj;
        if (this.inline != other.inline) {
            return false;
        }
        return Objects.equals(this.filename, other.filename);
    }

    /**
     * Format to a string showing all important attributes
     * 
     * @return String 
     */
    @Override
    public String toString() {
        return "\nAttachmentBean{\n" + "\tinline=" + inline + ", \n\tfilename=" + filename + ", \n\tbyteArray=" + Arrays.toString(byteArray) + ", \n\tcontentId=" + contentId + "\n}";
    }

    
    
    
    
    
    
    
}
