package com.dmanzano.email_application.util;

import javafx.scene.control.Alert;

/**
 * Utils used throughout the application
 * 
 * @author Danny
 * @version 3.6
 */
public final class JAGUtils {
        
    private JAGUtils(){}
    
    /**
     * Used in UI layer to display a message pop-up
     * 
     * @param title
     * @param content 
     */
    public static void displayAlertMessage(String title, String content){
        Alert dialog = new Alert(Alert.AlertType.INFORMATION);
        dialog.setTitle(title);
        dialog.setHeaderText(null);
        dialog.setContentText(content);
        dialog.showAndWait();
    }
    
}
