package com.dmanzano.email_application.constants;

/**
 *
 * @author Danny
 */
public enum RecipientType {
    TO, CC, BCC
}
