package com.dmanzano.email_application.ui;

import com.dmanzano.email_application.business.FetchNewEmails;
import com.dmanzano.email_application.data.ConfigProperties;
import com.dmanzano.email_application.util.JAGUtils;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Properties;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.event.EventType;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import jodd.mail.ImapServer;
import jodd.mail.MailServer;
import jodd.mail.ReceiveMailSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * FXML Controller class
 *
 * @author Danny
 * @version 3.6
 */
public class LoginFXMLController {

    private final static Logger LOG = LoggerFactory.getLogger(LoginFXMLController.class);

    private String imapServerName;
    private MailApp mailApp;

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="usernameInput"
    private TextField usernameInput; // Value injected by FXMLLoader

    @FXML // fx:id="passwordInput"
    private PasswordField passwordInput; // Value injected by FXMLLoaders

    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
        try {
            ConfigProperties cp = new ConfigProperties();
            Properties prop = cp.loadConfig();
            this.imapServerName = prop.getProperty("imapUrl");
        } catch (IOException e) {
            LOG.info(e.getMessage());
            JAGUtils.displayAlertMessage("Error initilizing the Login View", "This application is going to need to shut down");
            Platform.exit();
        }
    }

    /**
     * Checks if the configuration file has already a client logged in
     *
     */
    public void checkConfig() {
        try {
            ConfigProperties cp = new ConfigProperties();
            Properties prop = cp.loadConfig();
            if (prop.getProperty("uAddress").trim().length() != 0 && prop.getProperty("uPwd").trim().length() != 0) {
                openApp();
            }
        } catch (IOException e) {
            LOG.info(e.getMessage());
            JAGUtils.displayAlertMessage("Couldn't check for authentication", "This application is going to need to shut down");
            Platform.exit();
        }
    }

    /**
     * Login Button's click event handler
     *
     * @param event
     */
    @FXML
    void loginPressed(MouseEvent event) {
        checkLogin();
    }

    /**
     * Login's key pressed event handler
     *
     * @param event
     */
    @FXML
    void loginKeyPressed(KeyEvent event) {
        if (event.getCode() == KeyCode.ENTER) {
            checkLogin();
        }
    }

    /**
     * Tries to login with the given credentials And acts accordingly if
     * successful or not
     */
    private void checkLogin() {
        ImapServer is = MailServer.create()
                .host(this.imapServerName)
                .ssl(true)
                .auth(usernameInput.getText(), passwordInput.getText())
                .debugMode(true)
                .buildImapMailServer();

        try (ReceiveMailSession session = is.createSession()) {
            session.open();
            ConfigProperties cp = new ConfigProperties();
            cp.loadConfig();
            //set login value
            cp.setCredentials(usernameInput.getText().split("@")[0], usernameInput.getText(), passwordInput.getText());
            createTemp();
            openApp();
            LOG.info("Authentication success");
        } catch (Exception e) {
            LOG.info(e.getMessage());
            //Authentication failed
            LOG.info(e.getMessage());
            JAGUtils.displayAlertMessage("Authentication Failed", "Username or Password is invalid");
        }
        usernameInput.setText("");
        passwordInput.setText("");
    }

    /**
     * Creates temp directory if doesn't exists
     *
     */
    private void createTemp() {
        File f = new File("C:\\temp");
        if (!f.exists()) {
            f.mkdir();
        }
    }

    /**
     * Launches FetchNewEmails thread to get recent email
     *
     */
    private void openApp() {
        try {
            LOG.info("Open App");
            FetchNewEmails newThread = new FetchNewEmails(this.mailApp);
            newThread.start();
            this.mailApp.folderTreeController.setFolderTree();
            this.mailApp.setRootCenter(this.mailApp.splitView);
            this.mailApp.setRootTop(createMenuBar());
        } catch (IOException e) {
            LOG.info(e.getMessage());
            JAGUtils.displayAlertMessage("Couldn't open the application", "This application is going to need to shut down");
            Platform.exit();
        }
    }

    /**
     * Creates the MenuBar view
     *
     * @return MenuBar
     */
    private MenuBar createMenuBar() {

        MenuBar mb = new MenuBar();
        Menu config = new Menu("Configuration");
        MenuItem openConfig = new MenuItem("Open Configurations");
        openConfig.addEventHandler(EventType.ROOT, e -> {
            try {
                this.mailApp.configController.setConfig();
                this.mailApp.setRootCenter(this.mailApp.configView);
            } catch (IOException ex) {
                LOG.info(ex.getMessage());
                JAGUtils.displayAlertMessage("Failed to load the configuration file", "");
            }

        });
        config.getItems().add(openConfig);
        Menu account = new Menu("Account");
        MenuItem logout = new MenuItem("Logout");
        logout.addEventHandler(EventType.ROOT, e -> {
            ConfigProperties cp = new ConfigProperties();
            try {
                cp.setCredentials("", "", "");
                this.mailApp.mailListController.setTable("");
            } catch (IOException ex) {
                LOG.info(ex.getMessage());
                JAGUtils.displayAlertMessage("Error occured when you tried to logout", ex.getMessage());
            }
            this.mailApp.setRootCenter(this.mailApp.loginView);
            this.mailApp.unsetRootTop();
        });
        MenuItem refresh = new MenuItem("Refresh");
        refresh.addEventHandler(EventType.ROOT, e -> {
            try {
                FetchNewEmails fne = new FetchNewEmails(this.mailApp);
                fne.start();
            } catch (IOException ex) {
                LOG.info(ex.getMessage());
                JAGUtils.displayAlertMessage("Refresh Error", "Could not refresh your emails");
            }
        });
        account.getItems().add(refresh);
        account.getItems().add(logout);

        Menu help = new Menu("Help");
        MenuItem about = new MenuItem("About");
        about.addEventHandler(EventType.ROOT, e -> {
            Stage aboutStage = new Stage();
            WebView wv = new WebView();
            wv.getEngine().loadContent("<h1>Application functionalities:</h1>\n" +
"	<p>Login:</p>\n" +
"		<ul>\n" +
"			<li>Any gmail account can connect to the application in the condition that said email has enabled less secure applications</li>\n" +
"			<li>When clicking the login button it might take a few seconds to authenticate</li>\n" +
"			<li>When logged in there will always be an option to logout in the menu bar under account</li>\n" +
"		</ul>\n" +
"	<p>Mail Listing:</p>\n" +
"		<ul>\n" +
"			<li>On the left hand side is where the folders are listed and can be clicked on to list all emails under the folder which will show on the right hand side</li>\n" +
"			<li>Right clicking in the folder list view will give the option of creating a folder under a new name</li>\n" +
"			<li>Only the none default folders will give the option of deleting the folder and its content</li>\n" +
"			<li>The button compose will lead to the compose activity \\/ </li>\n" +
"		</ul>\n" +
"	<p>Composition:</p>\n" +
"		<ul>\n" +
"			<li>Allows to write a basic email (I mean basic in the sense that the embedded attachment functionality couldn't be implemented due to the fact that the task was almost impossible)</li>\n" +
"			<li>Invalid emails will not send the email</li>\n" +
"			<li>It also allows to add attachments to the email and delete them if necessary</li>\n" +
"			<li>The html editor allows you to write an html message</li>\n" +
"			<li>The email is sent asynchronously (meaning that it uses another thread) so you can do other things while its being sent</li>\n" +
"		</ul>\n" +
"	<p>Configuration:</p>\n" +
"		<ul>\n" +
"			<li>The activity shows the configuration file's information</li>\n" +
"			<li>It allows to modify the configuration file but it is advised not to because it could the application to malfunction</li>\n" +
"			<li>It doesn't allow to modify the user address and its password because they can only be set when logging in</li>\n" +
"			<li>If you click on cancel, it will bring you back to the last activity you were on</li>\n" +
"		</ul>\n" +
"	<p>Email Show:</p>\n" +
"		<ul>\n" +
"			<li>Shows the selected email's details</li>\n" +
"			<li>It shows the sender, the send date, the subject and the message</li>\n" +
"			<li>If the email has an html message, it will take priority over a text message and display it.</li>\n" +
"			<li>You have the option of deleting the email which will send you back to the Mail Listing activity and delete the email from the database</li>\n" +
"			<li>You can forward the message which will send you to the compose activity with all the proper fields of information set</li>\n" +
"			<li>You can reply or TEMP:reply all the message which will send you to the compose activity with all the proper fields of information set</li>\n" +
"		</ul>\n" +
"	<p>Menu Bar:</p>\n" +
"		<ul>\n" +
"			<li>The MenuBar is shown on every activity except the log in</li>\n" +
"			<li>The Configuration Menu has an \"Open Configuration\" menu item which opens the configuration activity</li>\n" +
"			<li>The Account Menu has a \"Refresh\" menu item which fetches new emails asynchronously and has a \"Logout\" menu item which logs you out.</li>\n" +
"			<li>The Help Menu has an \"About\" menu item which opens the about activity</li>\n" +
"		</ul>\n" +
"	\n" +
"	");
                Scene dialogScene = new Scene(wv, 840, 400);
                aboutStage.setScene(dialogScene);
                aboutStage.show();
        });
        help.getItems().add(about);
        mb.getMenus().addAll(config, account, help);

        return mb;
    }

    /**
     * Is called by the main application to give a reference back to itself.
     *
     * @param mailApp
     */

    public void setMainApp(MailApp mailApp) {
        this.mailApp = mailApp;
    }
}
