package com.dmanzano.email_application.ui;

import com.dmanzano.email_application.beans.AddressBean;
import com.dmanzano.email_application.beans.AttachmentBean;
import com.dmanzano.email_application.beans.EmailBean;
import com.dmanzano.email_application.business.LatePostSendDBUpdate;
import com.dmanzano.email_application.data.ConfigProperties;
import com.dmanzano.email_application.util.JAGUtils;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.event.EventType;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.web.HTMLEditor;
import javafx.stage.FileChooser;
import jodd.mail.RFC2822AddressParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Danny
 * @version 4.0
 */
public class ComposeViewFXMLController {

    private final static Logger LOG = LoggerFactory.getLogger(ComposeViewFXMLController.class);
    private MailApp mailApp;
    private final FileChooser fileChooser = new FileChooser();
    private String userAddress;
    private String userName;
    private List<AttachmentBean> fileList;
    private List<Button> attachButtonList;

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="toRecipients"
    public TextField toRecipients; // Value injected by FXMLLoader

    @FXML // fx:id="ccRecipients"
    public TextField ccRecipients; // Value injected by FXMLLoader

    @FXML // fx:id="bccRecipients"
    public TextField bccRecipients; // Value injected by FXMLLoader

    @FXML // fx:id="attachmentDisplay"
    private HBox attachmentDisplay; // Value injected by FXMLLoader

    @FXML // fx:id="subject"
    public TextField subject; // Value injected by FXMLLoader

    @FXML // fx:id="messageBody"
    public HTMLEditor messageBody; // Value injected by FXMLLoader

    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
        try {
            ConfigProperties cp = new ConfigProperties();
            Properties prop = cp.loadConfig();
            this.userAddress = prop.getProperty("uAddress");
            this.userName = prop.getProperty("uName");
            this.fileList = new ArrayList<>();
            this.attachButtonList = new ArrayList<>();
        } catch (IOException e) {
            LOG.info(e.getMessage());
            JAGUtils.displayAlertMessage("Error initilizing the Compose View", "This application is going to need to shut down");
            Platform.exit();
        }
    }

    /**
     * Back button's click event handler Returns to the folder and mail list
     * view
     *
     * @param event
     */
    @FXML
    void backPressed(MouseEvent event) {
        this.mailApp.setRootCenter(this.mailApp.splitView);
        setControlsToDefault();
        LOG.info("Back Button Pressed Returning to Mail List View");
    }

    /**
     * Attachment button's click event handler Uses FileChooser and stores the
     * files into an attachment list
     *
     * @param event
     */
    @FXML
    void attachmentPressed(MouseEvent event) {
        try {
            List<File> fileLs = fileChooser.showOpenMultipleDialog(this.mailApp.primaryStage);
            LOG.info("File Chooser Opened");
            if (fileLs != null) {
                AttachmentBean ab;
                for (File file : fileLs) {
                    ab = new AttachmentBean(file.getName(), Files.readAllBytes(file.toPath()), false, null);
                    LOG.info("File fetching: " + ab);
                    Button btn = new Button(ab.getFilename());
                    btn.addEventHandler(EventType.ROOT, e -> {
                        if (e.getEventType() == MouseEvent.MOUSE_CLICKED) {
                            if (e.getSource() instanceof Button) {
                                Button button = (Button) e.getSource();
                                for (int i = 0; i < this.fileList.size(); i++) {
                                    if (this.fileList.get(i).getFilename().equals(button.getText())) {
                                        this.fileList.remove(this.fileList.get(i));
                                    }
                                }
                                this.attachButtonList.remove(button);
                                this.attachmentDisplay.getChildren().remove(button);
                            }
                        }
                    });
                    this.fileList.add(ab);
                    this.attachButtonList.add(btn);
                    this.attachmentDisplay.getChildren().add(btn);
                }
            }
        } catch (IOException e) {
            JAGUtils.displayAlertMessage("The file(s) couldn't be loaded properly", "Please try again");
        }
    }

    /**
     * Send button's click event handler Launches LatePostSendDBUpdate thread to
     * work in the background
     *
     * @param event
     */
    @FXML
    void sendPressed(MouseEvent event) {
        //for debug
        LOG.info("Attachment Content: " + messageBody.getHtmlText());
        try {
            String[] tos = toRecipients.getText().split(";");
            String[] ccs = ccRecipients.getText().split(";");
            String[] bccs = bccRecipients.getText().split(";");

            EmailBean eb = new EmailBean(new AddressBean(this.userName, this.userAddress),
                    parseRecipients(tos), parseRecipients(ccs), parseRecipients(bccs),
                    subject.getText(), "", messageBody.getHtmlText(),
                    this.fileList.toArray(new AttachmentBean[this.fileList.size()]), 3, null, null, "SENT");
            LOG.info("Email From Compose: " + eb);
            //rebind view to null
            setControlsToDefault();

            //update the data base
            try {
                LatePostSendDBUpdate sendUpdate = new LatePostSendDBUpdate(eb, this.mailApp);
                sendUpdate.start();
            } catch (IOException e) {
                JAGUtils.displayAlertMessage("The email failed to send", "Please try again");
            }
            //Change to toast like pop up
            this.mailApp.setRootCenter(this.mailApp.splitView);
        } catch (IllegalArgumentException e) {
            LOG.info("Error in sending email: " + e.getMessage());
            JAGUtils.displayAlertMessage("Please try again", "Please validate your inputs");
        }
    }

    /**
     *
     * @param re
     * @return AddressBean[]
     */
    private AddressBean[] parseRecipients(String[] re) {
        List<AddressBean> ab = new ArrayList<>();
        LOG.info("Parse Recipients. re.length: " + re.length);
        for (int i = 0; i < re.length; i++) {
            LOG.info(re[i]);
            LOG.info((RFC2822AddressParser.STRICT.parseToEmailAddress(re[i].trim()) == null) + "");

            if (re[i].trim().length() != 0) {
                if (RFC2822AddressParser.STRICT.parseToEmailAddress(re[i].trim()) == null) {
                    throw new IllegalArgumentException("Email is not valid");
                }
                ab.add(new AddressBean("", re[i]));
            }
        }

        return ab.toArray(new AddressBean[ab.size()]);
    }

    /**
     * Cleans the view's input
     * 
     */
    private void setControlsToDefault() {
        this.fileList = new ArrayList<>();
        this.toRecipients.setText("");
        this.ccRecipients.setText("");
        this.bccRecipients.setText("");
        this.subject.setText("");
        this.messageBody.setHtmlText("");
        this.toRecipients.setDisable(false);
        this.ccRecipients.setDisable(false);
        this.bccRecipients.setDisable(false);
        this.subject.setDisable(false);
        this.messageBody.setDisable(false);
    }

    /**
     * Is called by the main application to give a reference back to itself.
     *
     * @param mailApp
     */
    public void setMainApp(MailApp mailApp) {
        this.mailApp = mailApp;
    }
}
