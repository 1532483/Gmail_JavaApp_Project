package com.dmanzano.email_application.ui;

import com.dmanzano.email_application.data.ConfigFXProperties;
import com.dmanzano.email_application.data.ConfigProperties;
import com.dmanzano.email_application.util.JAGUtils;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;

/**
 * 
 * @author Danny
 * @version 4.0
 */
public class ConfigurationViewFXMLController {

    private ConfigFXProperties cfxp;
    private ConfigProperties cp;
    private MailApp mailApp;

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="uName"
    private TextField uName; // Value injected by FXMLLoader

    @FXML // fx:id="uAddress"
    private TextField uAddress; // Value injected by FXMLLoader

    @FXML // fx:id="uPwd"
    private TextField uPwd; // Value injected by FXMLLoader

    @FXML // fx:id="smtpUrl"
    private TextField smtpUrl; // Value injected by FXMLLoader

    @FXML // fx:id="smtpPort"
    private TextField smtpPort; // Value injected by FXMLLoader

    @FXML // fx:id="imapUrl"
    private TextField imapUrl; // Value injected by FXMLLoader

    @FXML // fx:id="imapPort"
    private TextField imapPort; // Value injected by FXMLLoader

    @FXML // fx:id="jdbcUrl"
    private TextField jdbcUrl; // Value injected by FXMLLoader

    @FXML // fx:id="dbPort"
    private TextField dbPort; // Value injected by FXMLLoader

    @FXML // fx:id="sqlUName"
    private TextField sqlUName; // Value injected by FXMLLoader

    @FXML // fx:id="sqlUPwd"
    private TextField sqlUPwd; // Value injected by FXMLLoader

    @FXML // fx:id="dbName"
    private TextField dbName; // Value injected by FXMLLoader

    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
        try {
            ConfigProperties cp = new ConfigProperties();
            cfxp = new ConfigFXProperties(cp.loadConfig());
        } catch (IOException e) {
            JAGUtils.displayAlertMessage("Error initilizing the Configuration View", "This application is going to need to shut down");
            Platform.exit();
        }
    }
    
    /**
     * Sets binding of configuration
     * 
     * @throws IOException 
     */
    public void setConfig() throws IOException{
        cp = new ConfigProperties();
        cfxp = new ConfigFXProperties(cp.loadConfig());
        Bindings.bindBidirectional(uName.textProperty(), cfxp.uNameProperty());
        Bindings.bindBidirectional(uAddress.textProperty(), cfxp.uAddressProperty());
        Bindings.bindBidirectional(uPwd.textProperty(), cfxp.uPwdProperty());
        Bindings.bindBidirectional(smtpUrl.textProperty(), cfxp.smtpUrlProperty());
        Bindings.bindBidirectional(smtpPort.textProperty(), cfxp.smtpPortProperty());
        Bindings.bindBidirectional(imapUrl.textProperty(), cfxp.imapUrlProperty());
        Bindings.bindBidirectional(imapPort.textProperty(), cfxp.imapPortProperty());
        Bindings.bindBidirectional(jdbcUrl.textProperty(), cfxp.jdbcUrlProperty());
        Bindings.bindBidirectional(dbPort.textProperty(), cfxp.dbPortProperty());
        Bindings.bindBidirectional(sqlUName.textProperty(), cfxp.sqlUNameProperty());
        Bindings.bindBidirectional(sqlUPwd.textProperty(), cfxp.sqlUPwdProperty());
        Bindings.bindBidirectional(dbName.textProperty(), cfxp.dbNameProperty());
    }

    /**
     * Modify Button's click event handler Commits the configuration parameters'
     * change
     *
     * @param event
     */
    @FXML
    void modifyPressed(MouseEvent event) {
        try {
            cp.setAllConfig(uName.getText(), uAddress.getText(), uPwd.getText(), imapUrl.getText(),
                    smtpUrl.getText(), imapPort.getText(), smtpPort.getText(), jdbcUrl.getText(),
                    dbName.getText(), dbPort.getText(), sqlUName.getText(), sqlUPwd.getText());
            this.mailApp.setRootCenter(this.mailApp.previousNode);
        } catch (IOException e) {
            JAGUtils.displayAlertMessage("Modifying the configurations failed", "Please try again");
        }
    }

    /**
     * Cancel Button's click event handler Returns to the previous view
     *
     * @param event
     */
    @FXML
    void cancelPressed(MouseEvent event) {
        this.mailApp.setRootCenter(this.mailApp.previousNode);
    }

    /**
     * Is called by the main application to give a reference back to itself.
     *
     * @param mailApp
     */
    public void setMainApp(MailApp mailApp) {
        this.mailApp = mailApp;
    }
}
