package com.dmanzano.email_application.ui;

import com.dmanzano.email_application.util.JAGUtils;
import java.io.IOException;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.*;
import javafx.scene.Node;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 *
 * @author Danny
 * @version 4.0
 */
public class MailApp {

    private final static Logger LOG = LoggerFactory.getLogger(MailApp.class);
    public Stage primaryStage;

    //Layouts
    private BorderPane root;
    public HBox splitView;
    public VBox folderTree;
    public VBox mailList;
    public AnchorPane loginView;
    public VBox composeView;
    public AnchorPane configView;
    public Node previousNode;
    public VBox displayView;

    //Controllers
    public ComposeViewFXMLController composeController;
    public FolderTreeViewFXMLController folderTreeController;
    public LoginFXMLController loginController;
    public MailListViewFXMLController mailListController;
    public ConfigurationViewFXMLController configController;
    public EmailDisplayFXMLController displayController;

    /**
     * Instantiates the primary stage
     *
     * @param primaryStage
     */
    public MailApp(Stage primaryStage) {
        this.primaryStage = primaryStage;
    }

    /**
     * Starts the application
     *
     */
    public void start() {

        //Creating loaders
        FXMLLoader rootLoader = new FXMLLoader(MailApp.class.getResource("/fxml/RootFXML.fxml"));
        FXMLLoader splitLoader = new FXMLLoader(MailApp.class.getResource("/fxml/SplitViewFXML.fxml"));
        FXMLLoader folderTreeLoader = new FXMLLoader(MailApp.class.getResource("/fxml/FolderTreeViewFXML.fxml"));
        FXMLLoader mailListLoader = new FXMLLoader(MailApp.class.getResource("/fxml/MailListViewFXML.fxml"));
        FXMLLoader loginLoader = new FXMLLoader(MailApp.class.getResource("/fxml/LoginFXML.fxml"));
        FXMLLoader composeLoader = new FXMLLoader(MailApp.class.getResource("/fxml/ComposeViewFXML.fxml"));
        FXMLLoader configLoader = new FXMLLoader(MailApp.class.getResource("/fxml/ConfigurationViewFXML.fxml"));
        FXMLLoader displayLoader = new FXMLLoader(MailApp.class.getResource("/fxml/EmailDisplayFXML.fxml"));

        try {
            //Loading Layouts
            root = rootLoader.load();
            splitView = splitLoader.load();
            folderTree = folderTreeLoader.load();
            mailList = mailListLoader.load();
            loginView = loginLoader.load();
            composeView = composeLoader.load();
            configView = configLoader.load();
            displayView = displayLoader.load();
        } catch (IOException e) {
            JAGUtils.displayAlertMessage("Failed to load the layouts", "This application is going to need to shut down");
            Platform.exit();
        }

        //Loading Controllers
        mailListController = mailListLoader.getController();
        mailListController.setMainApp(this);
        folderTreeController = folderTreeLoader.getController();
        folderTreeController.setMainApp(this);
        composeController = composeLoader.getController();
        composeController.setMainApp(this);
        loginController = loginLoader.getController();
        loginController.setMainApp(this);
        configController = configLoader.getController();
        configController.setMainApp(this);
        displayController = displayLoader.getController();
        displayController.setMainApp(this);

        splitView.getChildren().add(folderTree);
        splitView.getChildren().add(mailList);

        root.setCenter(loginView);
        loginController.checkConfig();

        Scene scene = new Scene(root, 840, 640);

        primaryStage.setResizable(false);
        primaryStage.setTitle("JAG Application");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    /**
     *
     * @param n
     */
    public void setRootCenter(Node n) {
        if (this.root.getCenter() != n) {
            previousNode = this.root.getCenter();
            this.root.setCenter(n);
        }
    }

    /**
     *
     * @param n
     */
    public void setRootTop(Node n) {
        this.root.setTop(n);
    }

    /**
     * Unsets the top of root
     *
     */
    public void unsetRootTop() {
        this.root.setTop(null);
    }
}
