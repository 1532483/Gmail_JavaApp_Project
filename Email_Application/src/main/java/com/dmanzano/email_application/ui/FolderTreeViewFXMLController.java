package com.dmanzano.email_application.ui;

import com.dmanzano.email_application.persistence.EmailDAO;
import com.dmanzano.email_application.persistence.FolderDAO;
import com.dmanzano.email_application.util.JAGUtils;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextInputDialog;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Danny
 * @version 3.6
 */
public class FolderTreeViewFXMLController {

    private final static Logger LOG = LoggerFactory.getLogger(FolderTreeViewFXMLController.class);
    private MailApp mailApp;

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="folderTree"
    private ListView<String> folderTree; // Value injected by FXMLLoader

    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
            this.folderTree.setCellFactory(param -> new ColorRectangleCell());
    }

    /**
     * Compose Button's click event handler Opens compose view
     *
     * @param event
     */
    @FXML
    void composePressed(MouseEvent event) {
        //this.mailApp.root.setCenter(this.mailApp.composeView);
        this.mailApp.setRootCenter(this.mailApp.composeView);
    }

    /**
     * Fetches the folder from the DB and binds its tree to the mail list view
     *
     */
    public void setFolderTree() {
        try {
            FolderDAO dao = new FolderDAO();
            String[] folders = dao.getFolders();
            folderTree.setItems(FXCollections.observableArrayList(Arrays.asList(folders)));
            folderTree.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
                @Override
                public void changed(ObservableValue observable, Object oldValue,
                        Object newValue) {
                    try {
                        String folder = (String) newValue;
                        LOG.info("Setting folder: " + folder);
                        mailApp.mailListController.setTable(folder);
                    } catch (Exception e) {
                        LOG.info(e.getMessage());
                    }
                }

            });
            for (String s : folderTree.getItems()) {
                LOG.info(s);
            }
            this.folderTree.setContextMenu(getContextMenu());
        } catch (SQLException | IOException e) {
            LOG.info(e.getMessage());
            JAGUtils.displayAlertMessage("The folders failed to be retrieved", "The application will need to be closed");
            Platform.exit();
        }
    }

    private class ColorRectangleCell extends ListCell<String> {

        private final Label label = new Label();

        /**
         * Setting cells
         */
        public ColorRectangleCell() {
            ListCell thisCell = this;

            setOnDragOver(new EventHandler<DragEvent>() {
                @Override
                public void handle(DragEvent event) {
                    LOG.info("Object: " + event.getSource());
                    if (event.getGestureSource() != thisCell && event.getDragboard().hasString()) {
                        LOG.info("Drag over");
                        event.acceptTransferModes(TransferMode.MOVE);
                    }
                    event.consume();
                }

            });

            setOnDragEntered(new EventHandler<DragEvent>() {
                @Override
                public void handle(DragEvent event) {
                    //Add animation
                    if (event.getGestureSource() != thisCell && event.getDragboard().hasString() && !getItem().equals("INBOX") && !getItem().equals("SENT")) {
                        setOpacity(0.3);
                    }
                }

            });

            setOnDragExited(new EventHandler<DragEvent>() {
                @Override
                public void handle(DragEvent event) {
                    //Add animation
                    if (event.getGestureSource() != thisCell && event.getDragboard().hasString() && !getItem().equals("INBOX") && !getItem().equals("SENT")) {
                        setOpacity(1);
                    }
                }

            });

            setOnDragDropped(new EventHandler<DragEvent>() {
                @Override
                public void handle(DragEvent event) {
                    if (getItem() == null || getItem().equals("INBOX") || getItem().equals("SENT")) {
                        return;
                    }

                    Dragboard db = event.getDragboard();
                    boolean success = false;
                    if (db.hasString()) {
                        int eid = Integer.parseInt(db.getString());
                        LOG.info("Drag Done on email: " + eid);
                        try {
                            EmailDAO eDAO = new EmailDAO();
                            eDAO.updateEmailFolder(eid, getItem());
                            mailApp.mailListController.setTable(getItem());
                            mailApp.folderTreeController.folderTree.getSelectionModel().select(getItem());
                        } catch (Exception e) {
                            LOG.info(e.getMessage());
                            JAGUtils.displayAlertMessage("Error on Drag", "Couldn't move the email");
                        }
                        success = true;
                    }

                    event.setDropCompleted(success);
                    event.consume();
                }

            });

            setOnDragDone(DragEvent::consume);

        }

        /**
         * 
         * @param item
         * @param empty 
         */
        @Override
        protected void updateItem(String item, boolean empty) {
            super.updateItem(item, empty);
            if (empty || item == null) {
                setGraphic(null);
            } else {
                label.setText(item);
                this.setGraphic(label);
            }
        }

    }

    /**
     * 
     * @return ContextMenu 
     */
    public ContextMenu getContextMenu() {
        ContextMenu cm = new ContextMenu();
        MenuItem add = new MenuItem("Add Folder");
        add.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                TextInputDialog dialog = new TextInputDialog();
                dialog.setTitle("Add Folder");
                dialog.setContentText("Enter Folder Name:");
                Optional<String> result = dialog.showAndWait();
                result.ifPresent(name -> {
                    try {
                        FolderDAO fDAO = new FolderDAO();
                        fDAO.addFolder(name);
                        FolderTreeViewFXMLController.this.setFolderTree();
                    } catch (SQLException | IOException e) {
                        JAGUtils.displayAlertMessage("Please try again", "The folder couldn't be added");
                    }
                });
            }

        });

        cm.getItems().add(add);
        return cm;
    }

    /**
     * Is called by the main application to give a reference back to itself.
     *
     * @param mailApp
     */
    public void setMainApp(MailApp mailApp) {
        this.mailApp = mailApp;

    }

}
