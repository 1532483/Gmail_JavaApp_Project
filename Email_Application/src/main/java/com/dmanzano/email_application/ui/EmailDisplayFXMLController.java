package com.dmanzano.email_application.ui;

import com.dmanzano.email_application.beans.AttachmentFXBean;
import com.dmanzano.email_application.beans.EmailFXBean;
import com.dmanzano.email_application.persistence.EmailDAO;
import com.dmanzano.email_application.util.JAGUtils;
import java.awt.Desktop;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.net.URL;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.web.WebView;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.URI;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author Danny
 * @version 4.0
 */
public class EmailDisplayFXMLController {

    private final static Logger LOG = LoggerFactory.getLogger(EmailDisplayFXMLController.class);
    private WebView htmlMessageBody;
    private TextArea textMessageBody;
    private EmailFXBean email;
    private MailApp mailApp;
    private EmailDAO eDAO;
    private List<Button> attButton;

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="subjectLbl"
    private Label subjectLbl; // Value injected by FXMLLoader

    @FXML // fx:id="senderLbl"
    private Label senderLbl; // Value injected by FXMLLoader

    @FXML // fx:id="dateLbl"
    private Label dateLbl; // Value injected by FXMLLoader

    @FXML // fx:id="attachmentsLayout"
    private HBox attachmentsLayout; // Value injected by FXMLLoader

    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
        htmlMessageBody = new WebView();
        textMessageBody = new TextArea();
        attButton = new ArrayList<>();
    }

    /**
     * Delete Button's click event handler Removes the email from the DB and
     * goes back to mail list view
     *
     * @param event
     */
    @FXML
    void deletePressed(MouseEvent event) {
        try {
            this.eDAO.removeEmail(email.getEid());
            this.mailApp.setRootCenter(this.mailApp.splitView);
            this.mailApp.mailListController.setTable();
        } catch (SQLException e) {
            JAGUtils.displayAlertMessage("The email hasn't been deleted correctly", "Please try again");
        }
    }

    /**
     * Back Button's click event handler Changes back to the mail list view
     *
     * @param event
     */
    @FXML
    void goBackPressed(MouseEvent event) {
        this.mailApp.setRootCenter(this.mailApp.splitView);
    }

    /**
     * Forward Button's click event handler Adds forward tag to the message body
     * and subject And changes to the compose view
     *
     * @param event
     */
    @FXML
    void forwardPressed(MouseEvent event) {
        ComposeViewFXMLController cont = this.mailApp.composeController;
        cont.subject.setText("Fw: " + this.email.getSubject());
        if (this.email.getHtmlMessage().length() != 0) {
            cont.messageBody.setHtmlText("------Forwarded Message------</br>" + this.email.getHtmlMessage());
        } else {
            cont.messageBody.setHtmlText("------Forwarded Message------</br>"
                    + "<html dir=\"ltr\"><head></head><body contenteditable=\"true\">"
                    + this.email.getTextMessage()
                    + "</body></html>");
        }
        this.mailApp.setRootCenter(this.mailApp.composeView);
    }

    /**
     * Reply Button's click event handler Adds reply tag to the message body and
     * subject And changes to the compose view disabling inputs not needed in a
     * reply
     *
     * @param event
     */
    @FXML
    void replyPressed(MouseEvent event) {
        ComposeViewFXMLController cont = this.mailApp.composeController;
        cont.toRecipients.setText(this.email.getSender().getUserAddress());
        cont.subject.setText("Re: " + this.email.getSubject());
        cont.ccRecipients.setDisable(true);
        cont.toRecipients.setDisable(true);
        cont.bccRecipients.setDisable(true);
        if (this.email.getHtmlMessage().length() != 0) {
            cont.messageBody.setHtmlText("-------Replied Message------</br>"
                    + this.email.getHtmlMessage());
        } else {
            cont.messageBody.setHtmlText("-------Replied Message------"
                    + "<html dir=\"ltr\"><head></head><body contenteditable=\"true\">"
                    + this.email.getTextMessage() + "</body></html>");
        }

        this.mailApp.setRootCenter(this.mailApp.composeView);
    }

    /**
     * Binds the view with the right email
     *
     * @param eid
     */
    public void setBindings(int eid) {
        try {
            this.attachmentsLayout.getChildren().removeAll(attButton);

            eDAO = new EmailDAO();
            email = new EmailFXBean(eDAO.findEmailByEid(eid));
            subjectLbl.textProperty().bind(email.subjectProperty());
            senderLbl.textProperty().bind(email.senderProperty().getValue().userAddressProperty());
            dateLbl.textProperty().bind(new SimpleStringProperty(new SimpleDateFormat("MM/dd/yyyy, h:mma").format(email.sentDateProperty().getValue())));

            this.mailApp.displayView.getChildren().remove(this.htmlMessageBody);
            this.mailApp.displayView.getChildren().remove(this.textMessageBody);
            if (email.getHtmlMessage().trim().length() != 0) {
                try {
                    showHtml(email);
                } catch (Exception e) {
                    LOG.info(e.getMessage());
                }
            } else {
                showText(email);
            }

            List<AttachmentFXBean> attachments = new ArrayList<>();
            attButton = new ArrayList<>();
            ObservableList<AttachmentFXBean> abArr = email.getAttachments();
            if (abArr != null) {
                for (int i = 0; i < abArr.size(); i++) {
                    if (!abArr.get(i).getInline()) {
                        attachments.add(abArr.get(i));
                        Button b = new Button(abArr.get(i).getFilename());
                        b.addEventHandler(MouseEvent.MOUSE_CLICKED, e -> {
                            try {
                                Desktop.getDesktop().open(new File("C:\\temp\\" + ((Button) e.getSource()).getText()));
                            } catch (IOException ex) {
                                //do somethiing
                                ex.printStackTrace();
                            }
                        });
                        attButton.add(b);
                    }
                }
            }

            this.attachmentsLayout.getChildren().addAll(attButton);
        } catch (SQLException | IOException e) {
            JAGUtils.displayAlertMessage("The email content cannot be seen", "This email has not been set its bindings properly");
            this.mailApp.setRootCenter(this.mailApp.splitView);
        }

    }

    /**
     *
     * @param email
     */
    private void showText(EmailFXBean email) {
        LOG.info("Showing text for email");
        this.textMessageBody.setText(email.getTextMessage());
        this.textMessageBody.setDisable(true);
        this.textMessageBody.setStyle("-fx-opacity: 0.9;");
        this.mailApp.displayView.getChildren().add(this.textMessageBody);
    }

    /**
     * Write to a file and reads from it for WebView
     *
     * @param email
     * @throws IOException
     */
    private void showHtml(EmailFXBean email) throws IOException, ParserConfigurationException, SAXException, TransformerException {
        parseCidHTML();
        String htmlFile = "C:\\temp\\temp.html";
        writeHtmlToFile(htmlFile, email.getHtmlMessage());
        URI uri = java.nio.file.Paths.get(htmlFile).toAbsolutePath().toUri();
        htmlMessageBody.getEngine().load(uri.toString());
        this.mailApp.displayView.getChildren().add(htmlMessageBody);

    }
    
    private void parseCidHTML() throws IOException, ParserConfigurationException, SAXException, TransformerException {
        List<AttachmentFXBean> embedded = new ArrayList<>();
        for (AttachmentFXBean afxb : email.getAttachments()) {
            if (afxb.getInline()) {
                embedded.add(afxb);
            }
        }
        LOG.info(embedded.size() + "");
        DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        Document document = db.parse(new InputSource(new StringReader(email.getHtmlMessage())));
        NodeList list = document.getElementsByTagName("img");

        
        if (embedded.size() == list.getLength()) {
            for (int i = 0; i < list.getLength(); i++) {
                LOG.info(list.getLength() + "");
                for (int k = 0; k < embedded.size(); k++) {
                    if (list.item(i).getAttributes().getNamedItem("src").getNodeValue().equals("cid:" + embedded.get(k).getFilename())) {
                        list.item(i).getAttributes().getNamedItem("src").setNodeValue(embedded.get(k).getFilename());
                    }
                }
            }
        }
        
        DOMSource domSource = new DOMSource(document);
       StringWriter writer = new StringWriter();
       StreamResult result = new StreamResult(writer);
       TransformerFactory.newInstance().newTransformer().transform(domSource, result);
       LOG.info(writer.toString());
       
       email.setHtmlMessage(writer.toString());
    }

    /**
     *
     * @param htmlFile
     * @param html
     * @throws IOException
     */
    private void writeHtmlToFile(String htmlFile, String html) throws IOException {
        File f = new File(htmlFile);
        if (!f.exists()) {
            f.createNewFile();
        }

        try (Writer writer = new BufferedWriter(new FileWriter(f))) {
            writer.write(html);
        }
    }

    /**
     * Is called by the main application to give a reference back to itself.
     *
     * @param mailApp
     */
    public void setMainApp(MailApp mailApp) {
        this.mailApp = mailApp;
    }
}
