package com.dmanzano.email_application.ui;

import com.dmanzano.email_application.beans.EmailBean;
import com.dmanzano.email_application.beans.EmailFXBean;
import com.dmanzano.email_application.persistence.EmailDAO;
import com.dmanzano.email_application.persistence.FolderDAO;
import com.dmanzano.email_application.util.JAGUtils;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import javafx.scene.control.TableColumn;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * FXML Controller class
 *
 * @author Danny
 * @version 4.0
 */
public class MailListViewFXMLController {

    private final static Logger LOG = LoggerFactory.getLogger(MailListViewFXMLController.class);
    private MailApp mailApp;
    public String folder;

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="emailTable"
    private TableView<EmailFXBean> emailTable; // Value injected by FXMLLoader

    @FXML // fx:id="senderCol"
    private TableColumn<EmailFXBean, String> senderCol; // Value injected by FXMLLoader

    @FXML // fx:id="subjectCol"
    private TableColumn<EmailFXBean, String> subjectCol; // Value injected by FXMLLoader

    @FXML // fx:id="dateCol"
    private TableColumn<EmailFXBean, String> dateCol; // Value injected by FXMLLoader

    @FXML // fx:id="eid"
    private TableColumn<EmailFXBean, String> eid; // Value injected by FXMLLoader

    @FXML // fx:id="deleteBtn"
    private Button deleteBtn; // Value injected by FXMLLoader

    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
        //Initialize the email table
        senderCol.setCellValueFactory(bean -> bean.getValue().senderProperty().getValue().userAddressProperty());
        subjectCol.setCellValueFactory(bean -> bean.getValue().subjectProperty());
        dateCol.setCellValueFactory(bean -> {
            return new SimpleStringProperty(new SimpleDateFormat("MM/dd/yyyy").format(bean.getValue().sentDateProperty().getValue()));
        });
        eid.setCellValueFactory(bean -> bean.getValue().eidProperty().asString());
        folder = "";

        emailTable.addEventHandler(EventType.ROOT, e -> {
            if (e.getEventType() == MouseEvent.MOUSE_CLICKED) {
                if (e.getSource() instanceof TableView<?>) {
                    displayEmail(((TableView<EmailFXBean>) e.getSource()).getSelectionModel().getSelectedItem());
                }
            }
        });

        emailTable.setOnDragDetected(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                Dragboard db = emailTable.startDragAndDrop(TransferMode.MOVE);
                ClipboardContent content = new ClipboardContent();
                int eid = emailTable.getSelectionModel().getSelectedItem().getEid();
                content.putString(eid + "");

                LOG.info("Drag Detected on email: " + eid);
                db.setContent(content);
                event.consume();
            }

        });

        hideDeleteButton();
    }

    /**
     * Delete Button's click event handler
     *
     * @param event
     */
    @FXML
    void deletePressed(MouseEvent event) {
        try {
            FolderDAO fDAO = new FolderDAO();
            fDAO.removeFolder(folder);
            this.folder = "INBOX";
            setTable();
            this.mailApp.folderTreeController.setFolderTree();
        } catch (SQLException | IOException e) {
            JAGUtils.displayAlertMessage("Please try again", "The folder couldn't be deleted");
        }
    }

    /**
     * Displays display view and sets its binding
     *
     * @param email
     */
    public void displayEmail(EmailFXBean email) {
        if (email != null) {
            int eid = email.getEid();
            this.mailApp.displayController.setBindings(eid);
            this.mailApp.setRootCenter(this.mailApp.displayView);
        }
    }

    /**
     * Sets items' binding with specified parameter
     *
     * @param folder
     */
    public void setTable(String folder) {
        try {
            this.folder = folder;
            LOG.info("Setting folder: " + folder);
            if (this.folder.equals("INBOX") || this.folder.equals("SENT")) {
                hideDeleteButton();
            } else {
                showDeleteButton();
            }
            emailTable.setItems(getFoldersEmail(folder));
        } catch (SQLException | IOException e) {
            JAGUtils.displayAlertMessage("Unable to display the email from this folder", "Please choose another folder");
        }
    }

    /**
     * Makes the delete button invisible
     *
     */
    public void hideDeleteButton() {
        this.deleteBtn.setVisible(false);
    }

    /**
     * Makes the delete button visible
     *
     */
    public void showDeleteButton() {
        this.deleteBtn.setVisible(true);
    }

    /**
     * Sets items' binding with folder instance
     *
     */
    public void setTable() {
        setTable(this.folder);
    }

    /**
     *
     * @param folder
     * @return ObservableList<EmailFXBean>
     * @throws IOException
     * @throws SQLException
     */
    private ObservableList<EmailFXBean> getFoldersEmail(String folder) throws IOException, SQLException {
        EmailDAO emailDAO = new EmailDAO();
        EmailBean[] ebArr = emailDAO.findAllByFolder(folder);
        ArrayList<EmailFXBean> items = new ArrayList<>();
        for (int i = 0; i < ebArr.length; i++) {
            items.add(new EmailFXBean(ebArr[i]));
        }
        return FXCollections.observableArrayList(items);
    }

    /**
     * Is called by the main application to give a reference back to itself.
     *
     * @param mailApp
     */
    public void setMainApp(MailApp mailApp) {
        this.mailApp = mailApp;
    }

}
