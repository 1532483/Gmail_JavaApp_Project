package com.dmanzano.email_application.data;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

/**
 * Configuration Properties
 *
 * @author Danny
 * @version 3.6
 */
public class ConfigProperties {

    private final Properties prop;
    private final File f;
    private final String txtFile = "configuration.txt";

    /**
     * Instantiates private variables
     * 
     */
    public ConfigProperties() {
        f = new File(txtFile);
        prop = new Properties();
    }
    
    /**
     * Sets all parameters present in the configuration file
     * 
     * @param uName
     * @param uAddress
     * @param uPwd
     * @param imapUrl
     * @param smtpUrl
     * @param imapPort
     * @param smtpPort
     * @param jdbcUrl
     * @param dbName
     * @param dbPort
     * @param sqlUName
     * @param sqlUPwd
     * @throws IOException 
     */
    public void setAllConfig(String uName, String uAddress, String uPwd, String imapUrl,
            String smtpUrl, String imapPort, String smtpPort, String jdbcUrl, String dbName,
            String dbPort, String sqlUName, String sqlUPwd) throws IOException {
        createFileAndSetDefault();
        prop.setProperty("uName", uName);
        prop.setProperty("uAddress", uAddress);
        prop.setProperty("uPwd", uPwd);
        prop.setProperty("imapUrl", imapUrl);
        prop.setProperty("smtpUrl", smtpUrl);
        prop.setProperty("imapPort", imapPort);
        prop.setProperty("smtpPort", smtpPort);
        prop.setProperty("jdbcUrl", jdbcUrl);
        prop.setProperty("dbName", dbName);
        prop.setProperty("dbPort", dbPort);
        prop.setProperty("sqlUName", sqlUName);
        prop.setProperty("sqlUPwd", sqlUPwd);
        
        try (OutputStream propFileStream = new FileOutputStream(txtFile)) {
            prop.store(propFileStream, "Configuration Properties");
        }
    }

    /**
     * Create file and sets default configurations 
     * 
     * @throws IOException 
     */
    private void createFileAndSetDefault() throws IOException{
        if (!f.exists()) {
            f.createNewFile();
            //Sets the config file to default
            setAllConfig("", "", "", "imap.gmail.com", "smtp.gmail.com", "993", "465",
                    "jdbc:mysql://localhost:3306/JAGDB?autoReconnect=true&useSSL=false&allowPublicKeyRetrieval=true", 
                    "JAGDB", "3306", "creator", "1532483sro");
        }
    }
    
    /**
     * Sets only the first three params of the configuration file and the rest is set to default
     * 
     * @param uName
     * @param uAddress
     * @param uPwd
     * @throws IOException 
     */
    public void setCredentials(String uName, String uAddress, String uPwd) throws IOException{
        createFileAndSetDefault();
        setAllConfig(uName, uAddress, uPwd, "imap.gmail.com", "smtp.gmail.com", "993", "465",
                    "jdbc:mysql://localhost:3306/JAGDB?autoReconnect=true&useSSL=false&allowPublicKeyRetrieval=true", 
                    "JAGDB", "3306", "creator", "1532483sro");
    }

    /**
     * Load a properties with all the configuration parameters
     * 
     * @return Properties
     * @throws IOException 
     */
    public Properties loadConfig() throws IOException {
        createFileAndSetDefault();

        try (InputStream propFileStream = new FileInputStream(txtFile)) {
            prop.load(propFileStream);
        }

        return this.prop;
    }

}
