package com.dmanzano.email_application.data;

import com.dmanzano.email_application.business.UserInfo;
import com.dmanzano.email_application.beans.EmailBean;
import com.dmanzano.email_application.beans.AttachmentBean;
import java.io.IOException;
import java.util.Properties;
import jodd.mail.Email;
import jodd.mail.EmailAttachment;
import jodd.mail.MailServer;
import jodd.mail.SendMailSession;
import jodd.mail.SmtpServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * Handles sending the email to the server
 *
 * @author Danny
 * @version 3.3
 */
public class SmtpHandler {

    private final static Logger LOG = LoggerFactory.getLogger(SmtpHandler.class);

    private final String smtpServerName;
    private final String userAddress;
    private final String userPassword;

    /**
     * Create instance to have access to Smtp Server Name and performProtocol
     * method
     * 
     * @throws java.io.IOException
     */
    public SmtpHandler() throws IOException {
        ConfigProperties cp = new ConfigProperties();
        Properties prop = cp.loadConfig();
        this.smtpServerName = prop.getProperty("smtpUrl");
        this.userAddress = prop.getProperty("uAddress");
        this.userPassword = prop.getProperty("uPwd");
    }

    /**
     * Handles the send behavior and confirms through returning a boolean if
     * sending the email has been successful
     *
     * @param sender
     * @param email
     * @return boolean
     */
    @Deprecated
    public boolean performProtocol(UserInfo sender, EmailBean email) {
        return sendViaSmtpServer(getSmtpServer(sender), email);
    }

    /**
     * Handles the send behavior and confirms through returning a boolean if
     * sending the email has been successful 
     * Uses user from properties Config
     *
     * @param email
     * @return boolean
     */
    public boolean performProtocol(EmailBean email) {
        return sendViaSmtpServer(getSmtpServer(new UserInfo(this.userAddress, this.userPassword)), email);
    }

    /**
     * Returns an SmtpServer instance fully set with the sender credential
     *
     * @param sender
     * @return SmtpServer
     */
    private SmtpServer getSmtpServer(UserInfo sender) {
        return MailServer.create()
                .ssl(true)
                .host(this.smtpServerName)
                .auth(sender.getAddress(), sender.getPassword())
                .debugMode(true)
                .buildSmtpMailServer();
    }

    /**
     * Sends the email to the server
     * 
     * @param ss
     * @param email
     * @return boolean
     */
    private boolean sendViaSmtpServer(SmtpServer ss, EmailBean email) {
        try (SendMailSession sms = ss.createSession()) {
            sms.open();
            Email em = getEmail(email);
            sms.sendMail(em);
            LOG.info("Email has been succesfully sent");
            return true;
        } catch (Exception e) {
            LOG.info("Something went wrong. Unable to send email. "
                    + "\nError Message: " + e.getMessage());
            return false;
        }
    }

    /**
     * Builds the email with proper instance variables
     * 
     * @param eb
     * @return Email
     */
    public Email getEmail(EmailBean eb) {
        LOG.info("getEmail() in StmpHandler");
        Email email = Email.create();
        //Sender
        if (eb.getSender() != null) {
            email.from(eb.getSender().getEmailAddress());
            LOG.info("\tSenders set");
        }
        //Receiver
        if (eb.getReceiversEmailAddress().length != 0) {
            email.to(eb.getReceiversEmailAddress());
            LOG.info("\tReceivers set");
        }
        //CC
        if (eb.getCcsEmailAddress().length != 0) {
            email.cc(eb.getCcsEmailAddress());
            LOG.info("\tCC set");
        }
        //BCC
        if (eb.getBccsEmailAddress().length != 0) {
            email.bcc(eb.getBccsEmailAddress());
            LOG.info("\tbcc set");
        }
        //Subject
        if (eb.getSubject() != null) {
            email.subject(eb.getSubject());
            LOG.info("\tSubject set");
        }
        //Text Message
        if (eb.getTextMessage() != null) {
            email.textMessage(eb.getTextMessage());
            LOG.info("\tText Message set");
        }
        //Html Message
        if (eb.getHtmlMessage() != null) {
            email.htmlMessage(eb.getHtmlMessage());
            LOG.info("\tHtml Message set");
        }
        //Priority
        if (eb.getPriority() != -1) {
            email.priority(eb.getPriority());
            LOG.info("\tPriority set");
        }

        AttachmentBean[] abArr = eb.getAttachments();
        for (int i = 0; i < abArr.length; i++) {
            if (abArr[i].isInline()) {
                email = email.embeddedAttachment(EmailAttachment.with().content(abArr[i].getByteArray()).name(abArr[i].getFilename()));
            } else {
                email = email.attachment(EmailAttachment.with().content(abArr[i].getByteArray()).name(abArr[i].getFilename()));
            }
        }
        LOG.info("Email has been built.\t Exit the getEmail() method");
        return email;
    }
}
