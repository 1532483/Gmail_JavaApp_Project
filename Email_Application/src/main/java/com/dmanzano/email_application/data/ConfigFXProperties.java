package com.dmanzano.email_application.data;

import java.util.Properties;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author Danny
 * @version 3.4
 */
public class ConfigFXProperties {

    private StringProperty uName;
    private StringProperty uAddress;
    private StringProperty uPwd;
    private StringProperty smtpUrl;
    private StringProperty smtpPort;
    private StringProperty imapUrl;
    private StringProperty imapPort;
    private StringProperty jdbcUrl;
    private StringProperty dbPort;
    private StringProperty sqlUName;
    private StringProperty sqlUPwd;
    private StringProperty dbName;

    /**
     * Default Constructor
     */
    public ConfigFXProperties() {
        uName = new SimpleStringProperty("");
        uAddress = new SimpleStringProperty("");
        uPwd = new SimpleStringProperty("");
        smtpUrl = new SimpleStringProperty("");
        smtpPort = new SimpleStringProperty("");
        imapUrl = new SimpleStringProperty("");
        imapPort = new SimpleStringProperty("");
        jdbcUrl = new SimpleStringProperty("");
        dbPort = new SimpleStringProperty("");
        sqlUName = new SimpleStringProperty("");
        sqlUPwd = new SimpleStringProperty("");
        dbName = new SimpleStringProperty("");

    }

    /**
     * Sets according to property
     * @param p 
     */
    public ConfigFXProperties(Properties p) {
        uName = new SimpleStringProperty(p.getProperty("uName"));
        uAddress = new SimpleStringProperty(p.getProperty("uAddress"));
        uPwd = new SimpleStringProperty(p.getProperty("uPwd"));
        smtpUrl = new SimpleStringProperty(p.getProperty("smtpUrl"));
        smtpPort = new SimpleStringProperty(p.getProperty("smtpPort"));
        imapUrl = new SimpleStringProperty(p.getProperty("imapUrl"));
        imapPort = new SimpleStringProperty(p.getProperty("imapPort"));
        jdbcUrl = new SimpleStringProperty(p.getProperty("jdbcUrl"));
        dbPort = new SimpleStringProperty(p.getProperty("dbPort"));
        sqlUName = new SimpleStringProperty(p.getProperty("sqlUName"));
        sqlUPwd = new SimpleStringProperty(p.getProperty("sqlUPwd"));
        dbName = new SimpleStringProperty(p.getProperty("dbName"));
    }

    /**
     * 
     * @return StringProperty
     */
    public StringProperty uNameProperty() {
        return uName;
    }

    /**
     * 
     * @return String 
     */
    public String getUName() {
        return uName.getValue();
    }

    /**
     * 
     * @param uName 
     */
    public void setUName(String uName) {
        this.uName.setValue(uName);
    }

    /**
     * 
     * @return StringProperty
     */
    public StringProperty uAddressProperty() {
        return uAddress;
    }

    /**
     * 
     * @return String 
     */
    public String getUAddress() {
        return uAddress.getValue();
    }

    /**
     * 
     * @param uName 
     */
    public void setUAddress(String uAddress) {
        this.uAddress.setValue(uAddress);
    }

    /**
     * 
     * @return StringProperty
     */
    public StringProperty uPwdProperty() {
        return uPwd;
    }

    /**
     * 
     * @return String 
     */
    public String getUPwd() {
        return uPwd.getValue();
    }

    /**
     * 
     * @param uName 
     */
    public void setUPwd(String uPwd) {
        this.uPwd.setValue(uPwd);
    }

    /**
     * 
     * @return StringProperty
     */
    public StringProperty smtpUrlProperty() {
        return smtpUrl;
    }

    /**
     * 
     * @return String 
     */
    public String getsmtpUrl() {
        return smtpUrl.getValue();
    }

    /**
     * 
     * @param uName 
     */
    public void setSmtpUrl(String smtpUrl) {
        this.smtpUrl.setValue(smtpUrl);
    }

    /**
     * 
     * @return StringProperty
     */
    public StringProperty smtpPortProperty() {
        return smtpPort;
    }

    /**
     * 
     * @return String 
     */
    public String getSmtpPort() {
        return smtpPort.getValue();
    }

    /**
     * 
     * @param uName 
     */
    public void setSmtpPort(String smtpPort) {
        this.smtpPort.setValue(smtpPort);
    }

    /**
     * 
     * @return StringProperty
     */
    public StringProperty imapUrlProperty() {
        return imapUrl;
    }

    /**
     * 
     * @return String 
     */
    public String getImapUrl() {
        return imapUrl.getValue();
    }

    /**
     * 
     * @param uName 
     */
    public void setImapUrl(String imapUrl) {
        this.imapUrl.setValue(imapUrl);
    }

    /**
     * 
     * @return StringProperty
     */
    public StringProperty imapPortProperty() {
        return imapPort;
    }

    /**
     * 
     * @return String 
     */
    public String getImapPort() {
        return imapPort.getValue();
    }

    /**
     * 
     * @param uName 
     */
    public void setImapPort(String imapPort) {
        this.imapPort.setValue(imapPort);
    }

    /**
     * 
     * @return StringProperty
     */
    public StringProperty jdbcUrlProperty() {
        return jdbcUrl;
    }

    /**
     * 
     * @return String 
     */
    public String getJdbcUrl() {
        return jdbcUrl.getValue();
    }

    /**
     * 
     * @param uName 
     */
    public void setJdbcUrl(String jdbcUrl) {
        this.jdbcUrl.setValue(jdbcUrl);
    }

    /**
     * 
     * @return StringProperty
     */
    public StringProperty dbPortProperty() {
        return dbPort;
    }

    /**
     * 
     * @return String 
     */
    public String getDbPort() {
        return dbPort.getValue();
    }

    /**
     * 
     * @param uName 
     */
    public void setDbPort(String dbPort) {
        this.dbPort.setValue(dbPort);
    }

    /**
     * 
     * @return StringProperty
     */
    public StringProperty sqlUNameProperty() {
        return sqlUName;
    }

    /**
     * 
     * @return String 
     */
    public String getSqlUName() {
        return sqlUName.getValue();
    }

    /**
     * 
     * @param uName 
     */
    public void setSqlUName(String sqlUName) {
        this.sqlUName.setValue(sqlUName);
    }

    /**
     * 
     * @return StringProperty
     */
    public StringProperty sqlUPwdProperty() {
        return sqlUPwd;
    }

    /**
     * 
     * @return String 
     */
    public String getSqlUPwd() {
        return sqlUPwd.getValue();
    }

    /**
     * 
     * @param uName 
     */
    public void setSqlUPwd(String sqlUPwd) {
        this.sqlUPwd.setValue(sqlUPwd);
    }

    /**
     * 
     * @return StringProperty
     */
    public StringProperty dbNameProperty() {
        return dbName;
    }

    /**
     * 
     * @return String 
     */
    public String getDbName() {
        return dbName.getValue();
    }
    
    /**
     * 
     * @param uName 
     */
    public void setDbName(String dbName) {
        this.dbName.setValue(dbName);
    }
}
