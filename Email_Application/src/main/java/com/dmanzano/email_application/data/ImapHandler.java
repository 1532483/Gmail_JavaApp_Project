package com.dmanzano.email_application.data;

import com.dmanzano.email_application.beans.AddressBean;
import com.dmanzano.email_application.business.UserInfo;
import com.dmanzano.email_application.beans.EmailBean;
import com.dmanzano.email_application.beans.AttachmentBean;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Properties;
import java.sql.Timestamp;
import javax.activation.DataSource;
import javax.mail.Flags;
import jodd.mail.EmailAddress;
import jodd.mail.EmailAttachment;
import jodd.mail.EmailFilter;
import jodd.mail.EmailMessage;
import jodd.mail.ImapServer;
import jodd.mail.MailServer;
import jodd.mail.ReceiveMailSession;
import jodd.mail.ReceivedEmail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Handles the retrieving of email from the server
 *
 * @author Danny
 * @version 3.6
 */
public class ImapHandler {

    private final static Logger LOG = LoggerFactory.getLogger(ImapHandler.class);

    private final String imapServerName;
    private final String userAddress;
    private final String userPassword;
    private EmailBean[] emailBean;

    /**
     * Create instance to have access to Imap server name, userName, userAddress
     * and performProtocol method
     *
     * @throws java.io.IOException
     */
    public ImapHandler() throws IOException {
        ConfigProperties cp = new ConfigProperties();
        Properties prop = cp.loadConfig();
        this.imapServerName = prop.getProperty("imapUrl");
        this.userAddress = prop.getProperty("uAddress");
        this.userPassword = prop.getProperty("uPwd");
        LOG.info(imapServerName);
    }

    /**
     * Handles the receive behavior and confirms through returning a boolean if
     * reception of the email and population of the email beans have been
     * successful
     * @deprecated The methods encapsulated became public
     *
     * @param receiver
     * @return boolean
     */
    @Deprecated
    public boolean performProtocol(UserInfo receiver) {
        return getInboxUnseen(getImapServer(receiver));
    }
    
    /**
     *
     * @return EmailBean[]
     */
    public EmailBean[] getReceivedEmails() {
        return this.emailBean;
    }
    
    /**
     * Returns an ImapServer instance fully set with credentials
     * 
     * @param receiver
     * @return ImapServer
     */
    private ImapServer getImapServer() {
        return getImapServer(new UserInfo(this.userAddress, this.userPassword));
    }
    
    /**
     * Returns an ImapServer instance fully set with credentials
     *
     * @param receiver
     * @return ImapServer
     */
    private ImapServer getImapServer(UserInfo receiver) {
        return MailServer.create()
                .host(this.imapServerName)
                .ssl(true)
                .auth(receiver.getAddress(), receiver.getPassword())
                .debugMode(true)
                .buildImapMailServer();
    }

    /**
     * Receives all the unseen inbox emails
     *
     * @return boolean
     */
    public boolean getInboxUnseen(){
        return getInboxUnseen(getImapServer());
    }
    
    /**
     * Refer to its public overload
     * 
     * @param is
     * @return 
     */
    private boolean getInboxUnseen(ImapServer is) {
        try (ReceiveMailSession session = is.createSession()) {
            session.open();
            session.useFolder("INBOX");
            ReceivedEmail[] receivedEmail = session.receiveEmailAndMarkSeen(EmailFilter.filter().flag(Flags.Flag.SEEN, false));
            populateEmailBeanArray(receivedEmail, "INBOX");
            LOG.info("Reception and population of email has been successful");
            return true;
        } catch (Exception e) {
            LOG.info("Retrieve error. Unable to finish retrieving transaction."
                    + "Error Message: " + e.getMessage());
            throw new RuntimeException("Retrieve error. Unable to finish retrieving transaction."
                    + "Error Message: " + e.getMessage());
        }
    }
    
    /**
     * Receives all the recent inbox emails
     * 
     * @return boolean 
     */
    public boolean getInboxRecent(){
        return getInboxRecent(getImapServer());
    }
    
    /**
     * Refer to its public overload
     * 
     * @param is
     * @return 
     */
    private boolean getInboxRecent(ImapServer is){
        try (ReceiveMailSession session = is.createSession()) {
            session.open();
            session.useFolder("INBOX");
            ReceivedEmail[] receivedEmail = session.receiveEmailAndMarkSeen(EmailFilter.filter().flag(Flags.Flag.RECENT, true));
            populateEmailBeanArray(receivedEmail, "INBOX");
            LOG.info("Reception and population of email has been successful");
            return true;
        } catch (Exception e) {
            LOG.info("Retrieve error. Unable to finish retrieving transaction."
                    + "Error Message: " + e.getMessage());
            throw new RuntimeException("Retrieve error. Unable to finish retrieving transaction."
                    + "Error Message: " + e.getMessage());
        }
    }
    
    /**
     * Receives all the sent emails of user
     *
     * @return boolean
     */
    public boolean getSent(){
        return getSent(getImapServer());
    }
    
    /**
     * Refer to its public overload
     * 
     * @param is
     * @return 
     */
    private boolean getSent(ImapServer is) {
        try (ReceiveMailSession session = is.createSession()) {
            session.open();
            session.useFolder("[Gmail]/Sent Mail");
            ReceivedEmail[] receivedEmail = session.receiveEmail(EmailFilter.filter().from(this.userAddress));
            populateEmailBeanArray(receivedEmail, "SENT");
            LOG.info("Reception and population of email has been successful");
            return true;
        } catch (Exception e) {
            LOG.info("Retrieve error. Unable to finish retrieving transaction."
                    + "Error Message: " + e.getMessage());
            throw new RuntimeException("Retrieve error. Unable to finish retrieving transaction."
                    + "Error Message: " + e.getMessage());
        }
    }
    
    /**
     * Handles the receive behavior and confirms through returning a boolean if
     * reception of the email and population of the email beans have been
     * successful 
     * Uses user from Properties Configuration file 
     * Gets the sent email with the same subject
     *
     * @param subject
     * @return boolean
     */
    public boolean getSentSpecific(String subject){
        if(subject != null){
            return getSentSpecific(getImapServer(new UserInfo(this.userAddress, this.userPassword)), subject);
        }
        return false;
    }

    
    /**
     * Refer to its public overload
     *
     * @param is
     * @return
     */
    private boolean getSentSpecific(ImapServer is, String subject) {
        try (ReceiveMailSession session = is.createSession()) {
            session.open();
            session.useFolder("[Gmail]/Sent Mail");
            ReceivedEmail[] receivedEmail = session.receiveEmail(EmailFilter.filter().from(this.userAddress).and().subject(subject));
            populateEmailBeanArray(receivedEmail, "SENT");
            LOG.info("Reception and population of email has been successful");
            return true;
        } catch (Exception e) {
            LOG.info("Retrieve error. Unable to finish retrieving transaction."
                    + "Error Message: " + e.getMessage());
            return false;
        }
    }

    /**
     * Populates an EmailBean[] with a ReceivedEmail[]
     *
     * @param receivedEmail
     */
    private void populateEmailBeanArray(ReceivedEmail[] receivedEmail, String folder) {
        emailBean = new EmailBean[receivedEmail.length];
        ReceivedEmail re;
        for (int i = 0; i < emailBean.length; i++) {
            emailBean[i] = new EmailBean();
            re = receivedEmail[i];
            emailBean[i].setSender(new AddressBean(re.from().getPersonalName(), re.from().getEmail()));
            emailBean[i].setReceivers(parseEmailAddressToStringArray(re.to()));
            emailBean[i].setCcs(parseEmailAddressToStringArray(re.cc()));
            emailBean[i].setSubject(re.subject());
            //Only accept 1 message of each type according to specs
            emailBean[i].setTextMessage(parseTextMessages(re.messages()));
            emailBean[i].setHtmlMessage(parseHtmlMessages(re.messages()));
            //attachments
            emailBean[i].setAttachments(parseAttachments(re.attachments()));
            emailBean[i].setPriority(re.priority());
            emailBean[i].setSentDate(new Timestamp(re.sentDate().getTime()));
            emailBean[i].setReceivedDate(new Timestamp(re.receivedDate().getTime()));
            emailBean[i].setFolder(folder);
            LOG.info("Email Bean created from ReceivedEmail: \n" + emailBean[i].toString());

        }
    }

    /**
     *
     * @param lem
     * @return String
     */
    private String parseTextMessages(List<EmailMessage> lem) {
        String textMessage = "";
        for (int i = 0; i < lem.size(); i++) {
            if (lem.get(i).getMimeType().equals("TEXT/PLAIN")) {
                textMessage = lem.get(i).getContent();
                break;
            }
        }
        return textMessage;
    }

    /**
     *
     * @param lem
     * @return String
     */
    private String parseHtmlMessages(List<EmailMessage> lem) {
        String htmlMessage = "";
        for (int i = 0; i < lem.size(); i++) {
            if (lem.get(i).getMimeType().equals("TEXT/HTML")) {
                htmlMessage = lem.get(i).getContent();
                break;
            }
        }
        return htmlMessage;
    }

    /**
     *
     * @param lea
     * @return AttachmentBean[]
     */
    private AttachmentBean[] parseAttachments(List<EmailAttachment<? extends DataSource>> lea) {
        //Saving to disk
        if (lea != null) {
            lea.stream().forEachOrdered((attachment)->{
                attachment.writeToFile(
                        new File("c:\\temp", attachment.getName()));
            });
            
            
            AttachmentBean[] attach = new AttachmentBean[lea.size()];
            EmailAttachment ea;
            for (int i = 0; i < lea.size(); i++) {
                ea = lea.get(i);
                attach[i] = new AttachmentBean();
                attach[i].setFilename(ea.getName());
                attach[i].setInline(ea.isInline());
                attach[i].setContentId(ea.getContentId());
            }
            
        return attach;
        }
        return null;
    }

    /**
     *
     * @param ea
     * @return AddressBean[]
     */
    private AddressBean[] parseEmailAddressToStringArray(EmailAddress[] ea) {
        AddressBean[] strArr = new AddressBean[ea.length];
        for (int i = 0; i < strArr.length; i++) {

            strArr[i] = new AddressBean(ea[i].getPersonalName(), ea[i].getEmail());
            LOG.info("Address: " + strArr[i]);
        }

        return strArr;
    }

}
