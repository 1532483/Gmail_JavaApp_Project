package com.dmanzano.email_application.business;

import com.dmanzano.email_application.beans.EmailBean;
import com.dmanzano.email_application.data.ImapHandler;
import com.dmanzano.email_application.persistence.EmailDAO;
import com.dmanzano.email_application.ui.MailApp;
import java.io.IOException;
import java.sql.SQLException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Danny
 * @version 4.0
 */
public class FetchNewEmails extends Thread {
    private final static Logger LOG = LoggerFactory.getLogger(FetchNewEmails.class);

    private final MailApp mailApp;
    private final EmailDAO eDAO;
    private final ImapHandler imapHandler;

    /**
     * Instantiates private variables
     *
     * @param mailApp
     * @throws java.io.IOException
     */
    public FetchNewEmails(MailApp mailApp) throws IOException {
        this.mailApp = mailApp;
        this.eDAO = new EmailDAO();
        this.imapHandler = new ImapHandler();
    }

    /**
     * Fetches unseen email from the server in its own thread to optimize UI
     *
     */
    @Override
    public void run() {
        try {

            this.imapHandler.getInboxUnseen();
            EmailBean[] inboxArr = this.imapHandler.getReceivedEmails();
            EmailBean[] inboxDb = this.eDAO.findAllByFolder("INBOX");
            //Inserts inbox email fetched to the DB
            boolean inDB = false;
            for (int i = 0; i < inboxArr.length; i++) {
                for (int k = 0; k < inboxDb.length; k++) {
                    if (inboxArr[i].equals(inboxDb[k])) {
                        inDB = true;
                        break;
                    }
                }

                if (!inDB) {
                    this.eDAO.insertEmail(inboxArr[i]);
                }
                inDB = false;
            }
            
            if (this.mailApp.mailListController.folder.equals("INBOX")) {
                this.mailApp.mailListController.setTable("INBOX");
            }

            this.imapHandler.getSent();
            EmailBean[] sentArr = this.imapHandler.getReceivedEmails();
            EmailBean[] sentDb = this.eDAO.findAllByFolder("SENT");
            //Inserts sent email fetched to the DB
            for (int i = 0; i < sentArr.length; i++) {
                for (int k = 0; k < sentDb.length; k++) {
                    if (sentArr[i].equals(sentDb[k])) {
                        inDB = true;
                        break;
                    }
                }
                
                if (!inDB) {
                    this.eDAO.insertEmail(sentArr[i]);
                }
                inDB = false;
            }

            //Displays changes to the right folder if necessary
            if (this.mailApp.mailListController.folder.equals("SENT")) {
                this.mailApp.mailListController.setTable("SENT");
            }

        } catch (SQLException e) {
            e.printStackTrace();
            LOG.info("Error happened at FetchNewEmails thread. Message: " + e.getMessage());
        }
    }
}
