package com.dmanzano.email_application.business;

import jodd.mail.RFC2822AddressParser;

/**
 * Contains Account credentials
 *
 * @author Danny
 * @version 3.3
 */
public class UserInfo{

    private String address;
    private String pwd;

    /**
     * Instantiates an email account credentials
     * and adds a layer of validation.
     * Sets user name to default.
     * 
     * @param email
     * @param pwd
     * @throws IllegalArgumentException 
     */
    public UserInfo(String email, String pwd) {
        setEmail(email);
        setPassword(pwd);
    }
    

    /**
     * Return validated address
     *
     * @return String
     */
    public String getAddress() {
        return this.address;

    }

    /**
     *
     * @return String
     */
    public String getPassword() {
        return this.pwd;
    }

    /**
     * Use the RFC2822AddressParser to validate that the email string could be a
     * valid address
     *
     * @param address
     */
    private void setEmail(String address) {
        if (address != null && RFC2822AddressParser.STRICT.parseToEmailAddress(address) != null) {
            this.address = address;
        } else {
            throw new IllegalArgumentException("The account's address is invalid according to an email address' format standards");
        }
    }

    /**
     * 
     * @param pwd 
     */
    private void setPassword(String pwd) {
        if(pwd != null)
            this.pwd = pwd;
        else
            throw new IllegalArgumentException("The account's password is null");
    }

}
