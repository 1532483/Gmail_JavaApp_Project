package com.dmanzano.email_application.business;

import com.dmanzano.email_application.beans.EmailBean;
import com.dmanzano.email_application.data.ImapHandler;
import com.dmanzano.email_application.data.SmtpHandler;
import com.dmanzano.email_application.persistence.EmailDAO;
import com.dmanzano.email_application.ui.MailApp;
import java.io.IOException;
import java.sql.SQLException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Used for its multi-threading capabilities, Deals with the sending and
 * retrieving back of the email so it can be stored in the DB
 *
 * @author Danny
 * @version 4.0
 */
public class LatePostSendDBUpdate extends Thread {

    private final static Logger LOG = LoggerFactory.getLogger(LatePostSendDBUpdate.class);

    private final EmailDAO eDAO;
    private final ImapHandler imh;
    private final EmailBean sentEmailBean;
    private final MailApp mailApp;

    /**
     * Instantiates private variables
     *
     * @param eb
     * @param mailApp
     * @throws IOException
     */
    public LatePostSendDBUpdate(EmailBean eb, MailApp mailApp) throws IOException {
        this.sentEmailBean = eb;
        this.mailApp = mailApp;
        this.eDAO = new EmailDAO();
        this.imh = new ImapHandler();
    }

    /**
     * Runs the thread Sends and retrieves back for email server to store into
     * the DB Reasoning: the email has more information when being fetched from
     * the DB
     *
     */
    @Override
    public void run() {
        try {
            //Sending email to the server
            SmtpHandler sh = new SmtpHandler();
            boolean isSuccess = sh.performProtocol(this.sentEmailBean);
            if (isSuccess) {
                LOG.info("Smtp Protocol Succesful");
                try {
                    while (true) {
                        //Get sent emails with a speicfic subject to minimize REceivedEmails length
                        if (imh.getSentSpecific(this.sentEmailBean.getSubject())) {
                            LOG.info("Imap Protocol Succesful");

                            EmailBean[] ebArr = imh.getReceivedEmails();
                            if (ebArr.length != 0) {
                                EmailBean lastSent = ebArr[ebArr.length - 1];

                                if (sentEmailBean.equals(lastSent)) {
                                    LOG.info("Got the right email back");
                                    eDAO.insertEmail(lastSent);

                                    if (this.mailApp.mailListController.folder.equals("SENT")) {
                                        this.mailApp.mailListController.setTable("SENT");
                                    }
                                    break;

                                }
                            }
                        }
                    }
                    LOG.info("Email has been sent successfully");
                } catch (SQLException e) {
                    LOG.info("Error happened at LatePostSendDBUpdate thread. Message: " + e.getMessage());
                }
            }
        } catch (IOException e) {
            LOG.info("Error happened at LatePostSendDBUpdate thread. Message: " + e.getMessage());
        }
    }
    

}
