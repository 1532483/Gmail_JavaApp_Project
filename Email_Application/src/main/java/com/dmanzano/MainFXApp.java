/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dmanzano;

import com.dmanzano.email_application.ui.MailApp;
import javafx.application.Application;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Danny
 * @version 3.0
 */
public class MainFXApp extends Application {

    
    private final static Logger LOG = LoggerFactory.getLogger(MainFXApp.class);
    
    /**
     * Start the JAG App calling MailApp
     *
     * @param primaryStage
     */
    @Override
    public void start(Stage primaryStage) {
        MailApp ma = new MailApp(primaryStage);
        ma.start();
    }

    /**
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
