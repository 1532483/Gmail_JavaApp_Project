-- Created by Vertabelo (http://vertabelo.com)
-- Last modification date: 2018-09-18 22:11:41.056

DROP DATABASE IF EXISTS JAGDB;
CREATE DATABASE JAGDB;

USE JAGDB;

DROP USER IF EXISTS creator@localhost;
CREATE USER creator@'localhost' IDENTIFIED WITH mysql_native_password BY '1532483sro' REQUIRE NONE;
GRANT ALL ON JAGDB.* TO creator@'localhost';

FLUSH PRIVILEGES;

USE JAGDB;

DROP TABLE IF EXISTS ATTACHMENT;
DROP TABLE IF EXISTS RECIPIENT;
DROP TABLE IF EXISTS EMAIL;
DROP TABLE IF EXISTS FOLDER;


-- tables
-- Table: Attachments
CREATE TABLE Attachment (
    eid int NOT NULL,
    filename varchar(255) NOT NULL,
    contentId varchar(255),
    isInline bool NOT NULL
);

-- Table: Email
CREATE TABLE Email (
    eid int NOT NULL AUTO_INCREMENT,
    sender_address varchar(255) NOT NULL,
    sender_name varchar(255),
    subject varchar(255),
    text_message mediumtext NOT NULL,
    html_message mediumtext NOT NULL,
    priority int NOT NULL,
    sent_date timestamp NOT NULL,
    received_date timestamp,
    folder_id int NOT NULL,
    user_address varchar(255) NOT NULL,
    CONSTRAINT Email_pk PRIMARY KEY (eid)
);

-- Table: Folder
CREATE TABLE Folder (
    folder_id int NOT NULL AUTO_INCREMENT,
    folder_name varchar(255) NOT NULL,
    user_address varchar(255),
    CONSTRAINT Folder_pk PRIMARY KEY (folder_id)
);

-- Table: Recipient
CREATE TABLE Recipient (
    eid int NOT NULL,
    uaddress varchar(255) NOT NULL,
    uname varchar(255),
    relation varchar(255) NOT NULL
);

-- foreign keys
-- Reference: Attachments_Email (table: Attachments)
ALTER TABLE Attachment ADD CONSTRAINT Attachment_Email FOREIGN KEY Attachment_Email (eid)
    REFERENCES Email (eid) ON DELETE CASCADE;

-- Reference: Email_Folder (table: Email)
ALTER TABLE Email ADD CONSTRAINT Email_Folder FOREIGN KEY Email_Folder (folder_id)
    REFERENCES Folder (folder_id) ON DELETE CASCADE;

-- Reference: UserEmail_Email (table: UserEmail)
ALTER TABLE Recipient ADD CONSTRAINT Recipient_Email FOREIGN KEY Recipient_Email (eid)
    REFERENCES Email (eid) ON DELETE CASCADE;


INSERT INTO folder (folder_name) VALUES ('INBOX'), ('SENT');






