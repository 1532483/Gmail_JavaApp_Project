# Gmail_JavaApp_Project
<h1>Phase 4 Guidelines</h1>
<h3>Assumptions for running the application</h3>
	<ul>
	<li>MySQL should be installed on your computer and the database services should be set on netbeans as expected</li>
	<li>Please run the JAG_CREATE_DB.sql located in the 'Other Sources' folder</li>
	<li>Then run maven on the phase 3 goals</li>
	<li>There will be sample data due to the tests that will run automatically prior to the running of the application</li>
	<li>The logging is kept on in case an error happens and you can trace the error's source</li>
	</ul>
<h6>Maven goals: validate compile package verify install exec:java</h6>
<h6>Maven Goals Name: phase 3</h6>	

<h3>Known Issues Testing</h3> 
	<ul>
		<li>Some imap and stmp tests might fail if the internet connection is slow due to how the Thread.sleep waiting only 5 seconds between send and receive in the junit.
		If the internet conection is slow, the email might take more than 5 seconds to send to the server. Therefore the receiving will not be able the get the right email and the test will fail.</li>
		<li>The DAO tests cannot be run due to a flaw in ken's example which doesn't take into account having 1 DB for testing and 1 DB for the application. If I ask you to run both script that create the DBs, the DB that has been run first 
		will lose its ability to be connected to. Either the test fails or the app fails. So, I decided to keep the apps DB and ignore the DAO tests</li>
	</ul>
	
<h3>Known Issues for the application</h3>
	<ul>
		<li>Syntax errors in the html messages will cause the message not to show because I use a DOM Parsing to show embedded attachments with CIDs</li>
	</ul>	
	
<h1>Application functionalities:</h1>
	<p>Login:</p>
		<ul>
			<li>Any gmail account can connect to the application in the condition that said email has enabled less secure applications</li>
			<li>When clicking the login button it might take a few seconds to authenticate</li>
			<li>When logged in there will always be an option to logout in the menu bar under account</li>
		</ul>
	<p>Mail Listing:</p>
		<ul>
			<li>On the left hand side is where the folders are listed and can be clicked on to list all emails under the folder which will show on the right hand side</li>
			<li>Right clicking in the folder list view will give the option of creating a folder under a new name</li>
			<li>Only the none default folders will give the option of deleting the folder and its content</li>
			<li>The button compose will lead to the compose activity \/ </li>
		</ul>
	<p>Composition:</p>
		<ul>
			<li>Allows to write a basic email (I mean basic in the sense that the embedded attachment functionality couldn't be implemented due to the fact that the task was almost impossible)</li>
			<li>Invalid emails will not send the email</li>
			<li>It also allows to add attachments to the email and delete them if necessary</li>
			<li>The html editor allows you to write an html message</li>
			<li>The email is sent asynchronously (meaning that it uses another thread) so you can do other things while its being sent</li>
		</ul>
	<p>Configuration:</p>
		<ul>
			<li>The activity shows the configuration file's information</li>
			<li>It allows to modify the configuration file but it is advised not to because it could the application to malfunction</li>
			<li>It doesn't allow to modify the user address and its password because they can only be set when logging in</li>
			<li>If you click on cancel, it will bring you back to the last activity you were on</li>
		</ul>
	<p>Email Show:</p>
		<ul>
			<li>Shows the selected email's details</li>
			<li>It shows the sender, the send date, the subject and the message</li>
			<li>If the email has an html message, it will take priority over a text message and display it.</li>
			<li>You have the option of deleting the email which will send you back to the Mail Listing activity and delete the email from the database</li>
			<li>You can forward the message which will send you to the compose activity with all the proper fields of information set</li>
			<li>You can reply or TEMP:reply all the message which will send you to the compose activity with all the proper fields of information set</li>
		</ul>
	<p>Menu Bar:</p>
		<ul>
			<li>The MenuBar is shown on every activity except the log in</li>
			<li>The Configuration Menu has an "Open Configuration" menu item which opens the configuration activity</li>
			<li>The Account Menu has a "Refresh" menu item which fetches new emails asynchronously and has a "Logout" menu item which logs you out.</li>
			<li>The Help Menu has an "About" menu item which opens the about activity</li>
		</ul>
	
	
		    
