# Gmail_JavaApp_Project
<h1>Phase 4 Guidelines</h1>
<h3>Assumptions for running the application</h3>
	<ul>
	<li>MySQL should be installed on your computer and the database services should be set on netbeans as expected</li>
	<li>Please run the JAG_CREATE_DB.sql located in the 'Other Sources' folder</li>
	<li>Then run maven on the phase 3 goals</li>
	<li>There will be sample data due to the tests that will run automatically prior to the running of the application</li>
	<li>The logging is kept on in case an error happens and you can trace the error's source</li>
	</ul>
<h6>Maven goals: validate compile package verify install exec:java</h6>
<h6>Maven Goals Name: phase 3</h6>	

<h3>Known Issues Testing</h3> 
	<ul>
		<li>Some imap and stmp tests might fail if the internet connection is slow due to how the Thread.sleep waiting only 5 seconds between send and receive in the junit.
		If the internet conection is slow, the email might take more than 5 seconds to send to the server. Therefore the receiving will not be able the get the right email and the test will fail.</li>
		<li>The DAO tests cannot be run due to a flaw in ken's example which doesn't take into account having 1 DB for testing and 1 DB for the application. If I ask you to run both script that create the DBs, the DB that has been run first 
		will lose its ability to be connected to. Either the test fails or the app fails. So, I decided to keep the apps DB and ignore the DAO tests</li>
	</ul>
	
<h3>Known Issues for the application</h3>
	<ul>
		<li>Syntax errors in the html messages will cause the message not to show because I use a DOM Parsing to show embedded attachments with CIDs</li>
	</ul>	
	
		    
